## INSTALLATION

* `git clone <url repository>`
* Move to cloned repository `cd survey`
* `composer install`
* `cp .env.example .env`
* `php artisan key:generate`
* Change configurations database on **.env** file
<br>
`DB_CONNECTION=mysql`
<br>
`DB_HOST=<database host>`
<br>
`DB_PORT=<database port>`
<br>
`DB_DATABASE="<database name>"`
<br>
`DB_USERNAME="<database username>"`
<br>
`DB_PASSWORD=<database password>`
* `php artisan migrate` 
* `php artisan serve`

**Optional**
* Database seeder
<br>
User: `php artisan db:seed`
<br>
Questionnaire: `php artisan module:seed questionnaire`

**Others**
* `composer dumpautoload`
* `php artisan optimize:clear`
* `php artisan module:optimize`