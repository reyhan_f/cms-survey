<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'surveyor']);
        $admins = factory(App\User::class, 1)->create();
        foreach ($admins as $admin)
        {
            $admin->assignRole('admin');
        }

        $surveyor1 = User::create([
            'name' => 'Surveyor 1',
            'email' => 'surveyor1@survey.app',
            'phone' => '081111111111',
            'email_verified_at' => now(),
            'password' => bcrypt(12345678),
            'pin' => 123456,
            'remember_token' => Str::random(10),
        ]);
        $surveyor1->assignRole('surveyor');
        $surveyor2 = User::create([
            'name' => 'Surveyor 2',
            'email' => 'surveyor2@survey.app',
            'phone' => '082222222222',
            'email_verified_at' => now(),
            'password' => bcrypt(12345678),
            'pin' => 123456,
            'remember_token' => Str::random(10),
        ]);
        $surveyor2->assignRole('surveyor');
    }
}
