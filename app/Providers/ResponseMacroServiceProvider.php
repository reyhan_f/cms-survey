<?php

namespace App\Providers;

use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @param ResponseFactory $factory
     * @return void
     */
    public function boot(ResponseFactory $factory)
    {
        $factory->macro('api', function ($data = null, $response_code = 200, $message = 'OK') use ($factory) {
            $error = false;
            if($response_code != 200) {
                $error = true;
                if ($data instanceof Exception) {
                    $message = ($message != 'OK') ? $message : $data->getMessage();
                    if(env('APP_DEBUG') === false) {
                        $message = 'Something went wrong';
                    }
                }
            }

            $executionTime = number_format((float)((microtime(true) - LARAVEL_START)), 10, '.', '');

            $customFormat = [
                'meta' => [
                    'code' => $response_code,
                    'status' => $message,
                    'error' => $error,
                    'executionTime' => $executionTime.'s'
                ],
                'data' => (is_null($data)) ? (object) null : ((is_array($data)) ? (array) $data : (object) $data)
            ];
            return $factory->make($customFormat, $response_code)->withHeaders([
                'Content-Type' => 'application/json'
            ]);
        });
    }
}
