<?php

namespace App\Repositories;


use App\District;
use App\Province;
use App\Regency;
use App\Village;
use Illuminate\Database\Eloquent\Collection;

class LocationRepository {
    const PROVINCES = 'provinces';
    const REGENCIES = 'regencies';
    const DISTRICTS = 'districts';
    const VILLAGES = 'villages';
    const PROVINCE = 'province';
    const REGENCY = 'regency';
    const DISTRICT = 'district';
    const VILLAGE = 'village';

    /**
     * @param string $type
     * @param int|null $code
     * @return Province[]|array|Collection
     */
    public function getLocations(string $type, ?int $code)
    {
        switch ($type) {
            case self::PROVINCES:
                return $this->getProvinces();
            case self::REGENCIES:
                return $this->getRegencies($code);
            case self::DISTRICTS:
                return $this->getDistricts($code);
            case self::VILLAGES:
                return $this->getVillages($code);

            case self::PROVINCE:
                return $this->getProvince($code);
            case self::REGENCY:
                return $this->getRegency($code);
            case self::DISTRICT:
                return $this->getDistrict($code);
            case self::VILLAGE:
                return $this->getVillage($code);
            default:
                return array();
        }
    }

    public function getProvinces()
    {
        return Province::all();
    }

    public function getRegencies(int $code)
    {
        return Regency::where('province_id', $code)->get();
    }

    public function getDistricts(int $code)
    {
        return District::where('regency_id', $code)->get();
    }

    public function getVillages(int $code)
    {
        return Village::where('district_id', $code)->get();
    }

    public function getProvince(int $code)
    {
        return Province::find($code);
    }

    public function getRegency(int $code)
    {
        return Regency::find($code);
    }

    public function getDistrict(int $code)
    {
        return District::find($code);
    }

    public function getVillage(int $code)
    {
        return Village::find($code);
    }
}