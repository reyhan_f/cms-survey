<?php

use App\User;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

if (! function_exists('encrypt')) {
    /**
     * Function for encrypt string.
     *
     * @param string $string
     * @return string
     */
    function encrypting($string)
    {
        return Crypt::encrypt($string);
    }
}

if (! function_exists('decrypting')) {
    /**
     * Function for decrypt string.
     *
     * @param string $string
     * @return string
     */
    function decrypting($string)
    {
        try {
            return Crypt::decrypt($string);
        } catch (DecryptException $e) {
            toastr()->error('Failed to decrypt.', 'Error');
            return redirect()->back();
        }
    }
}

if (! function_exists('respondentNumber')) {
    /**
     * Function for generate Respondent Number.
     *
     * @param string $type
     * @param int $village
     * @param int $number
     * @return string
     */
    function respondentNumber(string $type, int $village, int $number)
    {
        return ucwords(substr($type, 0,1)).$village.str_pad($number, 4, '0', STR_PAD_LEFT);
    }
}

if (! function_exists('isAdmin')) {
    /**
     * Function for generate Respondent Number.
     *
     * @param null|int $user_id
     * @return boolean
     */
    function isAdmin($user_id = null)
    {
        if(is_null($user_id)) {
            $roles = auth()->user()->getRoleNames()->toArray();
        } else {
            $roles = User::find($user_id)->getRoleNames()->toArray();
        }
        return array_search('admin', $roles, true) !== false ? true : false;
    }
}

if (! function_exists('isSurveyor')) {
    /**
     * Function for generate Respondent Number.
     *
     * @param null|int $user_id
     * @return boolean
     */
    function isSurveyor($user_id = null)
    {
        if(is_null($user_id)) {
            $roles = auth()->user()->getRoleNames()->toArray();
        } else {
            $roles = User::find($user_id)->getRoleNames()->toArray();
        }
        return array_search('surveyor', $roles, true) !== false ? true : false;
    }
}

if (! function_exists('uploadImageBase64')) {
    /**
     * Function for generate Respondent Number.
     *
     * @param string $path
     * @param string $base64
     * @return boolean
     */
    function uploadImageBase64(string $path, string $base64)
    {
        $img = Image::make($base64)->encode('jpg');
        $hash = md5($img->__toString());
        $name = "{$hash}.jpg";
        Storage::disk('public')->put($path.DIRECTORY_SEPARATOR.$name, $img);

        return Storage::url($path.DIRECTORY_SEPARATOR.$name);
    }
}

if (! function_exists('getCategoryQuestion')) {
    /**
     * Function for get category id by survey type.
     *
     * @param string $type
     * @return string
     */
    function getCategoryQuestion(string $type)
    {
        switch ($type) {
            case 'komersil':
                return 0;
                break;
            case 'domestik-p':
                return 1;
                break;
            case 'domestik-np':
                return 2;
                break;
            default:
                return '';
                break;
        }
    }
}

