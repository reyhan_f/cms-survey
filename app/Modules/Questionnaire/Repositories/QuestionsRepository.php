<?php

namespace App\Modules\Questionnaire\Repositories;

use App\Modules\Questionnaire\Models\Answers;
use App\Modules\Questionnaire\Models\Category;
use App\Modules\Questionnaire\Models\Image as Photo;
use App\Modules\Questionnaire\Models\Question;
use App\Modules\Questionnaire\Models\Summary;
use App\Modules\Questionnaire\Models\SurveyResult;
use App\Modules\Questionnaire\Models\SurveyResultDetail;
use App\Regency;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Log;

class QuestionsRepository {

    protected $category;
    protected $question;
    protected $answers;
    protected $survey_result;
    protected $survey_result_detail;
    protected $summary;
    protected $photo;

    public function __construct()
    {
        $this->category = new Category;
        $this->question = new Question;
        $this->answers = new Answers;
        $this->survey_result = new SurveyResult;
        $this->survey_result_detail = new SurveyResultDetail;
        $this->summary = new Summary;
        $this->photo = new Photo;
    }

    /* GET DATA */
    /**
     * Function for select all question categories with questions and answers (options)
     *
     * @return Category|Builder
     */
    public function queryCategoryWithQuestionsAndAnswers()
    {
        return $this->category->with(['questions.answers']);
    }

    /**
     * Function for select all questions with answers (options)
     *
     * @return Question|Builder
     */
    public function queryQuestionsWithAnswers()
    {
        return $this->question->with(['answers']);
    }

    /**
     * Function for get all question categories with questions and answers (options)
     *
     * @return Category[]|Builder[]|Collection
     */
    public function getAllQuestions()
    {
        return $this->queryCategoryWithQuestionsAndAnswers()->get();
    }


    /**
     * Function for get all komersil question categories with questions and answers (options)
     *
     * @return Category[]|Builder[]|Collection
     */
    public function getAllQuestionsKomersil()
    {
        return $this->queryCategoryWithQuestionsAndAnswers()->where('type', $this->category::KOMERSIL)->get();
    }

    /**
     * Function for get all domestik question categories with questions and answers (options)
     *
     * @return Category[]|Builder[]|Collection
     */
    public function getAllQuestionsDomestik()
    {
        return $this->queryCategoryWithQuestionsAndAnswers()->where('type', 'like', $this->category::DOMESTIK.'%')->get();
    }

    /**
     * Function for get all questions with answers (options)
     *
     * @return Question[]|Builder[]|Collection
     */
    public function getQuestionsWithAnswers()
    {
        return $this->queryQuestionsWithAnswers()->get();
    }

    /**
     * function for get all survey results
     *
     * @param null|string $type
     * @return SurveyResult[]|Collection
     */
    public function getAllSurveyResult($type = null)
    {
        $user = auth()->user()->id;
        $survey_results = $this->survey_result->with(['regency.province', 'district', 'village']);

        if(! isAdmin($user)) {
            $survey_results = $survey_results->where('surveyor_id', $user);
        }

        if(! is_null($type))
        {
            return $survey_results->where('type', 'like', $type.'%')->get();
        }

        $survey_results = $survey_results->get();

        return $survey_results->map(function ($survey_result) {
            $survey_result->respondent_number_formated = respondentNumber($survey_result->type, (int) $survey_result->village->id, (int) $survey_result->respondent_number);
            return $survey_result;
        });
    }

    /**
     * function for get survey result
     *
     * @param int $id
     * @return SurveyResult[]|Collection
     */
    public function getSurveyResult($id) : array
    {
        $decrypted_id = decrypting($id);
        $result = $this->survey_result->with([
            'detail',
            'regency.province',
            'district',
            'village',
            'images'
        ])->find($decrypted_id);

        $respondent_number = respondentNumber($result->type, (int) $result->village->id, (int) $result->respondent_number);

        $data = $result->detail->map(function ($item) use ($result, $respondent_number) {
            $item->province = $result->regency->province->name;
            if($item->question_code === $this->question::KOTA) {
                $item->answer = $result->regency->name;
                return $item;
            }
            if($item->question_code === $this->question::KECAMATAN) {
                $item->answer = $result->district->name;
                return $item;
            }
            if($item->question_code === $this->question::KELURAHAN) {
                $item->answer = $result->village->name;
                return $item;
            }
            if($item->question_code === $this->question::NO_RESPONDENT_CODE) {
                $item->answer = $respondent_number;
                return $item;
            }
            return $item;
        })->groupBy('category');

        return [
            'type' => ucwords($result->type),
            'respondent_number' => $respondent_number,
            'places' => [
                'province' => $result->regency->province->name,
                'regency' => $result->regency->name,
                'district' => $result->district->name,
                'village' => $result->village->name,
            ],
            'data' => $data,
            'images' => $result->images,
        ];
    }

    public function getSummaries()
    {
        return $this->summary->with(['regency.province', 'district'])->get();
    }
    /* END GET DATA */

    /* SAVE DATA */
    /**
     * Function for saving survey result
     *
     * @param array $answers
     * @return array|string
     */
    public function storeSurveyResult(array $answers)
    {
        try {
            DB::beginTransaction();
            $unique_data_respondent = $this->getUniqueDataRespondentSurvey($answers);
            $other_data_respondent = $this->getOtherDataRespondentSurvey($answers);
            $survey_result = $this->survey_result->updateOrCreate($unique_data_respondent, $other_data_respondent);
            unset($answers['type']);
            $images = array();

            foreach ($answers as $question_code => $answer)
            {
                if($question_code === 'photos')
                {
                    $this->photo->where('survey_result_id', $survey_result->id)->delete();

                    foreach ($answer as $img)
                    {
                        $path = $unique_data_respondent['type'].DIRECTORY_SEPARATOR.$unique_data_respondent['kota'].DIRECTORY_SEPARATOR.$unique_data_respondent['kecamatan'].DIRECTORY_SEPARATOR.$unique_data_respondent['respondent_number'];
                        $img_path = uploadImageBase64($path, $img);
                        $arr_img_name = explode('/', $img_path);
                        $img_name = end($arr_img_name);
                        array_push($images, $img_name);
                        $this->photo->create([
                            'survey_result_id' => $survey_result->id,
                            'path' => $img_path
                        ]);
                    }
                } else {
                    $this->survey_result_detail->updateOrCreate([
                        'survey_result_id' => $survey_result->id,
                        'question_code' => $question_code
                    ],[
                        'answer' => $answer,
                    ]);
                }
            }

            $this->saveSummary($survey_result);

            DB::commit();
            return [
                'message' => 'Success',
                'images' => $images
            ];
        } catch (Exception $exception) {
            DB::rollback();
            Log::error($exception);
            return $exception->getMessage();
        }
    }

    /**
     * Function for saving survey result
     *
     * @param array $answers
     * @return array|string
     */
    public function storeSurveyResultV2(array $answers)
    {
        \Log::info($answers);
        try {
            DB::beginTransaction();
            $answers = $answers['data'];
            $unique_data_respondent = $this->getUniqueDataRespondentSurvey($answers);
            $other_data_respondent = $this->getOtherDataRespondentSurvey($answers);
            $survey_result = $this->survey_result->updateOrCreate($unique_data_respondent, $other_data_respondent);
            $type = getCategoryQuestion($answers['type']);
            unset($answers['type']);
            $images = array();

            foreach ($answers as $key => $answer)
            {
                \Log::info($key);
                switch ($key) {
                    case 'results':
                        foreach ($answer as $result) {
                            \Log::info($result['code']);
                            $question = $this->question
                                ->with('category')
                                ->whereHas('category', function ($query) use ($type) {
                                    $query->where('type', $type);
                                })
                                ->where('code', $result['code'])
                                ->first();
                            $ans = $result['answer'];
                            if($result['type'] == $this->question::MULTIPLE_CHOICES) {
                                $wer = $this->answers::where('question_id', $question->id)->where('key', $ans)->first();
                                $ans = (@$wer) ? $wer->value : null;
                            }
                            $this->survey_result_detail->updateOrCreate([
                                'survey_result_id' => $survey_result->id,
                                'question_code' => $result['code'],
                            ],[
                                'answer' => $ans,
                                'question' => (@$question->question) ?: null,
                                'category' => (@$question->category->full_name) ?: null
                            ]);
                        }
                        break;
                    case 'photos':
                        $this->photo->where('survey_result_id', $survey_result->id)->delete();
                        foreach ($answer as $img)
                        {
                            $path = $unique_data_respondent['type'].DIRECTORY_SEPARATOR
                                .$unique_data_respondent['kota'].DIRECTORY_SEPARATOR
                                .$unique_data_respondent['kecamatan'].DIRECTORY_SEPARATOR
                                .$unique_data_respondent['respondent_number'];
                            $img_path = uploadImageBase64($path, $img);
                            $arr_img_name = explode('/', $img_path);
                            $img_name = end($arr_img_name);
                            array_push($images, $img_name);
                            $this->photo->create([
                                'survey_result_id' => $survey_result->id,
                                'path' => $img_path
                            ]);
                        }
                        break;
                    default:
                        $this->survey_result_detail->updateOrCreate([
                            'survey_result_id' => $survey_result->id,
                            'question_code' => $key,
                        ],[
                            'answer' => $answer,
                            'question' => null,
                            'category' => 'Tambahan'
                        ]);
                        break;
                }
            }

            $this->saveSummary($survey_result);

            DB::commit();
            return [
                'message' => 'Success',
                'images' => $images
            ];
        } catch (Exception $exception) {
            DB::rollback();
            Log::error($exception);
            return $exception->getMessage();
        }
    }

    /**
     * Saving summary
     *
     * @param SurveyResult $survey_result
     * @return void
     */
    public function saveSummary(SurveyResult $survey_result) {
        $wasCreated = $survey_result->wasRecentlyCreated;

        $regency = Regency::with('province')->find($survey_result['kota']);
        $province = $regency->province->id;

        $summary_data = [
            'province_id' => $province,
            'regency_id' => $survey_result['kota'],
            'district_id' => $survey_result['kecamatan'],
            'village_id' => null,
        ];

        $summary = $this->summary->firstOrCreate($summary_data);

        if($survey_result['type'] == 'domestik' && $wasCreated)
        {
            $summary->increment('domestik');
        }

        if($survey_result['type'] == 'komersil' && $wasCreated)
        {
            $summary->increment('komersil');
        }
    }

    /**
     * Function for formatting all data respondent
     *
     * @param array $answers
     * @return array
     */
    private function getAllDataRespondentSurvey(array $answers)
    {
        $unique_data = $this->getUniqueDataRespondentSurvey($answers);
        $other_data = $this->getOtherDataRespondentSurvey($answers);

        return array_merge($unique_data, $other_data);
    }

    /**
     * Function for formatting unique data respondent
     *
     * @param array $answers
     * @return array
     */
    private function getUniqueDataRespondentSurvey(array $answers)
    {
        $results = collect($answers['results']);
        $respondent_number = $results->where('code', $this->question::NO_RESPONDENT_CODE)->first();
        $kota = $results->where('code', $this->question::KOTA)->first();
        $kecamatan = $results->where('code', $this->question::KECAMATAN)->first();
        $kelurahan = $results->where('code', $this->question::KELURAHAN)->first();
        $surveyor_id = auth()->user()->id;
        $type = $answers[$this->question::TYPE];

        if(!$respondent_number['answer']) {
            $res = $this->survey_result->select('type', 'kota', 'respondent_number')
            ->where([
                ['type', 'like', strtolower(substr($type, 0,4)).'%'],
                ['kota', $kota['answer']],
            ])
            ->orderBy('respondent_number', 'desc')
            ->first();
            $respondent_number = ($res) ? ($res->respondent_number + 1) : 1;
        }

        return [
            'respondent_number' => $respondent_number,
            'kota' => (@$kota['answer']) ? $kota['answer'] : null,
            'kecamatan' => (@$kecamatan['answer']) ? $kecamatan['answer'] : null,
            'kelurahan' => (@$kelurahan['answer']) ? $kelurahan['answer'] : null,
            'surveyor_id' => (@$surveyor_id) ? $surveyor_id : null,
            'type' => (@$type) ? $type : null
        ];
    }

    /**
     * Function for formatting other data respondent
     *
     * @param array $answers
     * @return array
     */
    private function getOtherDataRespondentSurvey(array $answers)
    {
        $results = collect($answers['results']);
        $name = $results->where('code', $this->question::NAME)->first();
        $gender = $results->where('code', $this->question::GENDER)->first();
        $age = $results->where('code', $this->question::AGE)->first();
        $phone = (ucwords($answers['type']) === strtolower($this->category::KOMERSIL_STR))
            ? $results->where('code', $this->question::PHONE_KOMERSIL)->first()
            : $results->where('code', $this->question::PHONE_DOMESTIK)->first();
        $address = (strpos($answers['type'], strtolower($this->category::DOMESTIK_STR)) !== false)
            ? $results->where('code', $this->question::ADDRESS_KOMERSIL)->first()
            : $results->where('code', $this->question::ADDRESS_DOMESTIK)->first();
        $education = $results->where('code', $this->question::EDUCATION)->first();
        $position = $results->where('code', $this->question::POSITION)->first();
        $gps = $answers[$this->question::GPS];
        $timestamp = $answers[$this->question::TIMESTAMP];

        $g = $this->answers::where('question_id', 9)->where('key', $gender['answer'])->first()->value ?: null;
        $e = $this->answers::where('question_id', 11)->where('key', $education['answer'])->first()->value ?: null;
        $p = $this->answers::where('question_id', 12)->where('key', $position['answer'])->first()->value ?: null;
        return [
            'name' => (@$name['answer']) ? $name['answer'] : null,
            'gender' => (@$gender['answer'])
                ? $g
                : (@$gender['answer']),
            'age' => (@$age['answer']) ? $age['answer'] : null,
            'phone' => (@$phone['answer']) ? $phone['answer'] : null,
            'address' => (@$address['answer']) ? $address['answer'] : null,
            'education' => (@$education['answer'])
                ? $e
                : (@$education['answer']),
            'position' => (@$position['answer'])
                ? $p
                : (@$position['answer']),
            'gps' => (@$gps) ? $gps : null,
            'timestamp' => Carbon::parse($timestamp)->format('Y-m-d H:i:s'),
        ];
    }
    /* END SAVE DATA */
}
