<?php

namespace App\Modules\Questionnaire\Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Modules\Questionnaire\Models\Answers;
use Faker\Generator as Faker;

$factory->define(Answers::class, function (Faker $faker) {
    return [
        'question_id' => $faker->unique()->randomNumber(3, true),
        'key' => $faker->word,
        'value' => $faker->words(3, true)
    ];
});
