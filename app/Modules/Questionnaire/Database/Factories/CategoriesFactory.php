<?php

namespace App\Modules\Questionnaire\Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Modules\Questionnaire\Models\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'code' => $faker->word,
        'name' => $faker->words(3, true)
    ];
});
