<?php

namespace App\Modules\Questionnaire\Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Modules\Questionnaire\Models\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'category_id' => $faker->unique()->randomNumber(2, true),
        'code' => $faker->word,
        'question' => $faker->words(3, true),
        'type' => $faker->randomElement([0, 1])
    ];
});
