<?php

namespace App\Modules\Questionnaire\Database\Seeds;

use App\Modules\Questionnaire\Models\Answers;
use App\Modules\Questionnaire\Models\Category;
use App\Modules\Questionnaire\Models\Question;
use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Category::class, 9)
            ->create()
            ->each(function ($category) {
                $category->questions()->saveMany(factory(Question::class, 10)->make());
                $category->questions()->each(function($question) {
                    $question->answers()->saveMany(factory(Answers::class, 4)->make());
                });
            });
    }
}
