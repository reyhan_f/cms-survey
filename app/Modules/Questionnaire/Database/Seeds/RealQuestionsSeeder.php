<?php

namespace App\Modules\Questionnaire\Database\Seeds;

use App\Modules\Questionnaire\Models\Answers;
use App\Modules\Questionnaire\Models\Category;
use App\Modules\Questionnaire\Models\Question;
use Illuminate\Database\Seeder;

class RealQuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Komersil */
        $komersil_categories = config('komersil_categories');
        $komersil_questions = config('komersil_questions');
        $komersil_answers = config('komersil_answers');

        foreach ($komersil_categories as $category)
        {
            $category = Category::create($category);
            foreach ($komersil_questions[$category->code] as $question)
            {
                $question['category_id'] = $category->id;
                $question = Question::create($question);
                if($question->type === 1) {
                    foreach ($komersil_answers[$question->code] as $answer) {
                        $answer['question_id'] = $question->id;
                        Answers::create($answer);
                    }
                }
            }
        }
        /* End Komersil */

        /* Domestik Pelanggan*/
        $domestik_pelanggan_categories = config('domestik_pelanggan_categories');
        $domestik_pelanggan_questions = config('domestik_pelanggan_questions');
        $domestik_pelanggan_answers = config('domestik_pelanggan_answers');

        foreach ($domestik_pelanggan_categories as $category)
        {
            $category = Category::create($category);
            foreach ($domestik_pelanggan_questions[$category->code] as $question)
            {
                $question['category_id'] = $category->id;
                $question = Question::create($question);
                if($question->type === 1) {
                    foreach ($domestik_pelanggan_answers[$question->code] as $answer) {
                        $answer['question_id'] = $question->id;
                        Answers::create($answer);
                    }
                }
            }
        }
        /* End Domestik Pelanggan*/

        /* Domestik Non Pelanggan*/
        $domestik_non_pelanggan_categories = config('domestik_non_pelanggan_categories');
        $domestik_non_pelanggan_questions = config('domestik_non_pelanggan_questions');
        $domestik_non_pelanggan_answers = config('domestik_non_pelanggan_answers');

        foreach ($domestik_non_pelanggan_categories as $category)
        {
            $category = Category::create($category);
            foreach ($domestik_non_pelanggan_questions[$category->code] as $question)
            {
                $question['category_id'] = $category->id;
                $question = Question::create($question);
                if($question->type === 1) {
                    foreach ($domestik_non_pelanggan_answers[$question->code] as $answer) {
                        $answer['question_id'] = $question->id;
                        Answers::create($answer);
                    }
                }
            }
        }
        /* End Non Pelanggan */
    }
}
