<?php

namespace App\Modules\Questionnaire\Database\Seeds;

use Illuminate\Database\Seeder;

class QuestionnaireDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(QuestionsSeeder::class);
        $this->call(RealQuestionsSeeder::class);
    }
}
