<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summaries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('district_id')->index();
            $table->unsignedBigInteger('regency_id')->index();
            $table->unsignedBigInteger('province_id')->index();
            $table->unsignedBigInteger('village_id')->nullable();
            $table->unsignedBigInteger('komersil')->default(0);
            $table->unsignedBigInteger('domestik')->default(0);
            $table->unsignedBigInteger('target')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summaries');
    }
}
