<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->index();
            $table->unsignedBigInteger('kota')->index();
            $table->unsignedBigInteger('kecamatan')->index();
            $table->unsignedBigInteger('kelurahan')->index();
            $table->unsignedBigInteger('respondent_number')->index();
            $table->string('name');
            $table->string('gender');
            $table->integer('age');
            $table->string('phone');
            $table->string('address');
            $table->string('education');
            $table->string('position');
            $table->string('gps');
            $table->timestamp('timestamp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_results');
    }
}
