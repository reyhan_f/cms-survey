<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuestionToSurveyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('survey_result_details', function (Blueprint $table) {
            $table->text('category')->after('survey_result_id')->nullable()->default(null);
            $table->text('question')->after('question_code')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('survey_result_details', function (Blueprint $table) {
            $table->dropColumn('category');
            $table->dropColumn('question');
        });
    }
}
