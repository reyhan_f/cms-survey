<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyResultDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_result_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('survey_result_id');
            $table->string('question_code');
            $table->string('answer')->nullable();
            $table->timestamps();
        });

        Schema::table('survey_result_details', function (Blueprint $table) {
            $table->foreign('survey_result_id')
                ->references('id')
                ->on('survey_results')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->index(['survey_result_id', 'question_code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_result_details');
    }
}
