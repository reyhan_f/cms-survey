<?php

namespace App\Modules\Questionnaire\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('questionnaire', 'Resources/Lang', 'app'), 'questionnaire');
        $this->loadViewsFrom(module_path('questionnaire', 'Resources/Views', 'app'), 'questionnaire');
        $this->loadMigrationsFrom(module_path('questionnaire', 'Database/Migrations', 'app'), 'questionnaire');
        $this->loadConfigsFrom(module_path('questionnaire', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('questionnaire', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
