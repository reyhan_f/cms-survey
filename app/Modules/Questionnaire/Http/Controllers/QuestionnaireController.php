<?php

namespace App\Modules\Questionnaire\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Questionnaire\Repositories\QuestionsRepository;

class QuestionnaireController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new QuestionsRepository;
    }

    public function result($type)
    {
        return view('questionnaire::result', compact('type'));
    }

    public function show($type, $id)
    {
        $data = $this->repo->getSurveyResult($id);
        $respondent_number = $data['respondent_number'];
        $places = $data['places'];
        $result = $data['data'];
        $images = $data['images'];
        return view('questionnaire::show', compact('type', 'respondent_number', 'places', 'result', 'images'));
    }

    public function summary()
    {
        return view('questionnaire::summary');
    }
}
