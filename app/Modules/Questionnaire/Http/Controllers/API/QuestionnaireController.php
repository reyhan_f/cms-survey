<?php

namespace App\Modules\Questionnaire\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Modules\Questionnaire\Http\Requests\API\StoreRequest;
use App\Modules\Questionnaire\Repositories\QuestionsRepository;
use Exception;
use Illuminate\Http\JsonResponse;

class QuestionnaireController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new QuestionsRepository;
    }

    public function list()
    {
        $questionnaire = $this->repo->queryCategoryWithQuestionsAndAnswers()
            ->paginate(10, ['*'], 'code');

        return response()->json([
            'status' => 'OK',
            'data' => $questionnaire
        ], 200);
    }

    /**
     * Function for save survey result
     *
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $answers = $request->all();
        $data = $this->repo->storeSurveyResult($answers);

        return response()->json([
            'message' => $data['message'],
            'images' => $data['images']
        ], 200);
    }

    /**
     * Datatable survey results
     *
     * @param string $type
     *
     * @return mixed
     * @throws Exception
     */
    public function datatableResult($type)
    {
        $result = $this->repo->getAllSurveyResult($type);
        return datatables()->of($result)
            ->editColumn('respondent_number', function ($data) {
                return respondentNumber($data->type, (int) $data->village->id, (int) $data->respondent_number);
            })
            ->editColumn('provinsi', function ($data) {
                return $data->regency->province->name;
            })
            ->editColumn('kota', function ($data) {
                return $data->regency->name;
            })
            ->editColumn('kecamatan', function ($data) {
                return $data->district->name;
            })
            ->editColumn('kelurahan', function ($data) {
                return $data->village->name;
            })
            ->editColumn('type', function ($data) {
                return ucwords(explode('-', $data->type)[0]);
            })
            ->addColumn('action', function ($data) use($type) {
                return '<a href="'.route('questionnaire.result.show', ['type' => $type, 'id' => encrypt($data->id)]).'" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>';
            })
            ->rawColumns(['action'])
            ->removeColumn('id')
            ->removeColumn('regency')
            ->removeColumn('district')
            ->removeColumn('village')
            ->toJson();
    }

    /**
     * Datatable summaries
     *
     * @return mixed
     * @throws Exception
     *
     */
    public function datatableSummary()
    {
        $summaries = $this->repo->getSummaries();
        return datatables()->of($summaries)
            ->addColumn('provinsi', function ($data) {
                return ucwords($data->province->name);
            })
            ->addColumn('kota', function ($data) {
                return ucwords($data->regency->name);
            })
            ->addColumn('kecamatan', function ($data) {
                return ucwords($data->district->name);
            })
            ->editColumn('komersil', function ($data) {
                return $data->komersil.' / '.$data->target;
            })
            ->editColumn('domestik', function ($data) {
                return $data->domestik.' / '.$data->target;
            })
            ->removeColumn('id')
            ->removeColumn('regency')
            ->removeColumn('district')
            ->removeColumn('village')
            ->removeColumn('province_id')
            ->removeColumn('regency_id')
            ->removeColumn('district_id')
            ->removeColumn('village_id')
            ->toJson();
    }
}
