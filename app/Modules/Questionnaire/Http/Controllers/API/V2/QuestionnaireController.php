<?php

namespace App\Modules\Questionnaire\Http\Controllers\API\V2;

use App\Http\Controllers\Controller;
use App\Modules\Questionnaire\Repositories\QuestionsRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Log;

class QuestionnaireController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new QuestionsRepository;
    }

    public function list()
    {
        $questionnaire = $this->repo->queryCategoryWithQuestionsAndAnswers()
            ->paginate(10, ['*'], 'code');

        return response()->json([
            'status' => 'OK',
            'data' => $questionnaire
        ], 200);
    }

    /**
     * Function for save survey result
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $answers = $request->all();
            $data = $this->repo->storeSurveyResultV2($answers);

            return response()->json([
                'message' => $data['message'],
                'images' => $data['images']
            ], 200);
        } catch (Exception $exception) {
            Log::error($exception);
            return response()->json([
                'message' => 'Internal server error.'
            ], 500);
        }
    }

}
