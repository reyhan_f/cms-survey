<?php

namespace App\Modules\Questionnaire\Http\Requests\API;

use App\Http\Requests\JsonRequest;

class StoreRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (request('type')) {
            case 'komersil':
                return $this->komersil();
            case 'domestik':
                return $this->domestik();
            default:
                return $this->komersil();
        }
    }

    private function komersil()
    {
        return [
            'A01' => 'required|integer',
            'A02' => 'required|integer',
            'A03' => 'required|integer',
            'A04' => 'required|integer',
            'A05' => 'required',
            'A06' => 'required',
            'A07' => 'required',
            'B01' => 'required',
            'B02' => 'required',
            'B03' => 'required|integer',
            'B04' => 'required',
            'B05' => 'required',
            'C01' => 'required',
            'C02' => 'required',
            'C03' => 'nullable',
            'C04' => 'nullable',
            'C05' => 'nullable',
            'C06' => 'nullable',
            'C07' => 'nullable',
            'D01' => 'required',
            'D02' => 'nullable',
            'D03' => 'nullable',
            'D04' => 'nullable',
            'D05' => 'nullable',
            'D06' => 'nullable',
            'D07' => 'nullable',
            'D08' => 'nullable',
            'D09' => 'nullable',
            'D10' => 'nullable',
            'D11' => 'nullable',
            'D12' => 'nullable',
            'D13' => 'nullable',
            'D14' => 'nullable',
            'D15' => 'nullable',
            'D16' => 'nullable',
            'E01' => 'nullable',
            'F01' => 'nullable',
            'F02' => 'nullable',
            'F03' => 'nullable',
            'F04' => 'nullable',
            'coordinate' => 'required',
            'photos' => 'required|array',
        ];
    }

    private function domestik()
    {
        return [
            'A01' => 'required|integer',
            'A02' => 'required|integer',
            'A03' => 'required|integer',
            'A04' => 'required|integer',
            'A05' => 'required',
            'A06' => 'required',
            'B01' => 'required',
            'B02' => 'required',
            'B03' => 'required|integer',
            'B04' => 'required',
            'B05' => 'required',
            'B06' => 'required',
            'C01' => 'required',
            'C02' => 'required',
            'C03' => 'nullable',
            'C04' => 'nullable',
            'C05' => 'nullable',
            'C06' => 'nullable',
            'D01' => 'required',
            'D02' => 'nullable',
            'D03' => 'nullable',
            'D04' => 'nullable',
            'D05' => 'nullable',
            'D06' => 'nullable',
            'D07' => 'nullable',
            'D08' => 'nullable',
            'D09' => 'nullable',
            'D10' => 'nullable',
            'D11' => 'nullable',
            'D12' => 'nullable',
            'D13' => 'nullable',
            'D14' => 'nullable',
            'D15' => 'nullable',
            'D16' => 'nullable',
            "D17" => 'nullable',
            'D18' => 'nullable',
            'D19' => 'nullable',
            'D20' => 'nullable',
            'D21' => 'required',
            'D22' => 'nullable',
            'D23' => 'nullable',
            'D24' => 'nullable',
            'D25' => 'nullable',
            'D26' => 'required',
            'D27' => 'nullable',
            'D28' => 'nullable',
            'D29' => 'nullable',
            'D30' => 'nullable',
            'D31' => 'required',
            'D32' => 'required',
            'D33' => 'nullable',
            'D34' => 'required',
            'D35' => 'required',
            'D36' => 'nullable',
            'D37' => 'required',
            'D38' => 'required',
            'D39' => 'nullable',
            'D40' => 'required',
            'D41' => 'required',
            'D42' => 'required',
            'D43' => 'required',
            'D44' => 'nullable',
            'E01' => 'required',
            'E02' => 'nullable',
            'E03' => 'nullable',
            'coordinate' => 'required',
            'photos' => 'required|array',
        ];
    }
}
