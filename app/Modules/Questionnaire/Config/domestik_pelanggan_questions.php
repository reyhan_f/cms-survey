<?php
return [
    'A' => [
        [
            'code' => 'A01',
            'question' => 'Kota',
            'type' => 0
        ],
        [
            'code' => 'A02',
            'question' => 'Kecamatan',
            'type' => 0
        ],
        [
            'code' => 'A03',
            'question' => 'Kelurahan',
            'type' => 0
        ],
        [
            'code' => 'A04',
            'question' => 'No. Urut Responden',
            'type' => 0
        ],
        [
            'code' => 'A05',
            'question' => 'Alamat Lengkap',
            'type' => 0
        ],
        [
            'code' => 'A06',
            'question' => 'No. Telp',
            'type' => 0
        ]
    ],
    'B' => [
        [
            'code' => 'B01',
            'question' => 'Nama Responden',
            'type' => 0
        ],
        [
            'code' => 'B02',
            'question' => 'Jenis Kelamin',
            'type' => 1
        ],
        [
            'code' => 'B03',
            'question' => 'Usia',
            'type' => 0
        ],
        [
            'code' => 'B04',
            'question' => 'Tingkat Pendidikan Terakhir',
            'type' => 1
        ],
        [
            'code' => 'B05',
            'question' => 'Pekerjaan responden',
            'type' => 1
        ],
        [
            'code' => 'B06',
            'question' => 'Status dalam rumah tangga',
            'type' => 1
        ],
    ],
    'C' => [
        [
            'code' => 'C01',
            'question' => 'Nama Kepala Keluarga',
            'type' => 0
        ],
        [
            'code' => 'C02',
            'question' => 'Jenis Kelamin',
            'type' => 1
        ],
        [
            'code' => 'C03',
            'question' => 'Usia',
            'type' => 0
        ],
        [
            'code' => 'C04',
            'question' => 'Tingkat Pendidikan Terakhir',
            'type' => 1
        ],
        [
            'code' => 'C05',
            'question' => 'Pekerjaan responden',
            'type' => 1
        ],
        [
            'code' => 'C06',
            'question' => 'Jumlah anggota rumah tangga?',
            'type' => 0
        ],
    ],
    'D' => [
        [
            'code' => 'D01',
            'question' => 'Status kepemilikan rumah',
            'type' => 1
        ],
        [
            'code' => 'D02',
            'question' => 'Luas tanah? (m<sup>2</sup>)',
            'type' => 0
        ],
        [
            'code' => 'D03',
            'question' => 'Luas bangunan? (m<sup>2</sup>)',
            'type' => 0
        ],
        [
            'code' => 'D04',
            'question' => 'Kondisi bangunan',
            'type' => 1
        ],
        [
            'code' => 'D05',
            'question' => 'Kondisi lingkungan',
            'type' => 1
        ],
        [
            'code' => 'D06',
            'question' => 'Lebar akses jalan depan rumah',
            'type' => 1
        ],
        [
            'code' => 'D07',
            'question' => 'Daya listrik',
            'type' => 1
        ],
        [
            'code' => 'D08',
            'question' => 'Peruntukan rumah',
            'type' => 1
        ]
    ],
    'E' => [
        [
            'code' => 'E01',
            'question' => 'Berapa total pendapatan rumah tangga?',
            'type' => 0
        ],
    ],
    'F' => [
        [
            'code' => 'F01',
            'question' => 'Sudah berapa lama rumah tangga ini menggunakan PDAM?',
            'type' => 1
        ],
        [
            'code' => 'F02',
            'question' => 'Apakah yang menggunakan sambungan PDAM ini?',
            'type' => 1
        ],
        [
            'code' => 'F03',
            'question' => 'Apakah penggunaan sambungan PDAM ini?',
            'type' => 1
        ],
        [
            'code' => 'F04',
            'question' => 'Kategori tarif/m<sup>3</sup> (Rupiah)',
            'type' => 0
        ],
        [
            'code' => 'F05',
            'question' => 'Berapa kubik rata-rata penggunaan air/bulan selama tiga bulan terakhir? (m<sup>3</sup>/bulan)',
            'type' => 0
        ],
        [
            'code' => 'F06',
            'question' => 'Berapa rata-rata pembayaran rekening air/bulan selama tiga bulan terakhir (Rupiah/bulan)',
            'type' => 0
        ],
        [
            'code' => 'F07',
            'question' => 'Bagaimana pendapat Bapak/Ibu dengan harga tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F08',
            'question' => 'Berapa jam dalam sehari pelayanan PDAM beroperasi? ',
            'type' => 1
        ],
        [
            'code' => 'F09',
            'question' => 'Apakah pasokan air PDAM mencukupi kebutuhan air rumah tangga sehari - hari?',
            'type' => 1
        ],
        [
            'code' => 'F10',
            'question' => 'Bagaimana kualitas air dari PDAM?',
            'type' => 1
        ],
        [
            'code' => 'F11',
            'question' => 'Apakah anda puas dengan pelayanan PDAM?',
            'type' => 1
        ],
        [
            'code' => 'F12',
            'question' => 'Alasan tidak puas dengan pelayanan PDAM?',
            'type' => 1
        ],
        [
            'code' => 'F13',
            'question' => 'Apakah anda pernah menyampaikan keluhan ke PDAM?',
            'type' => 1
        ],
        [
            'code' => 'F14',
            'question' => 'Jika pernah, bagaimana respon dari pihak PDAM?',
            'type' => 1
        ],
        [
            'code' => 'F15',
            'question' => 'Apa manfaat yang dirasakan sejak berlangganan PDAM?',
            'type' => 1
        ],
        [
            'code' => 'F16',
            'question' => 'Apa yang anda akan sarankan ke PDAM agar pelayanannya lebih baik?',
            'type' => 1
        ],
        [
            'code' => 'F17',
            'question' => 'Apa rumah tangga ini masih menggunakan sumber air lain?',
            'type' => 1
        ],
        [
            'code' => 'F18',
            'question' => 'Alasan masih digunakannya sumber lain',
            'type' => 1
        ],
        [
            'code' => 'F19',
            'question' => 'Apakah rumah tangga ini memiliki bak penampungan air (toren air)?',
            'type' => 1
        ],
        [
            'code' => 'F20',
            'question' => 'Kapasitas total bak penampungan air? (liter)',
            'type' => 0
        ]
    ]
];
