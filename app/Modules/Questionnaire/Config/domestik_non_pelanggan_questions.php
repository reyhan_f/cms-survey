<?php
return [
    'A' => [
        [
            'code' => 'A01',
            'question' => 'Kota',
            'type' => 0
        ],
        [
            'code' => 'A02',
            'question' => 'Kecamatan',
            'type' => 0
        ],
        [
            'code' => 'A03',
            'question' => 'Kelurahan',
            'type' => 0
        ],
        [
            'code' => 'A04',
            'question' => 'No. Urut Responden',
            'type' => 0
        ],
        [
            'code' => 'A05',
            'question' => 'Alamat Lengkap',
            'type' => 0
        ],
        [
            'code' => 'A06',
            'question' => 'No. Telp',
            'type' => 0
        ]
    ],
    'B' => [
        [
            'code' => 'B01',
            'question' => 'Nama Responden',
            'type' => 0
        ],
        [
            'code' => 'B02',
            'question' => 'Jenis Kelamin',
            'type' => 1
        ],
        [
            'code' => 'B03',
            'question' => 'Usia',
            'type' => 0
        ],
        [
            'code' => 'B04',
            'question' => 'Tingkat Pendidikan Terakhir',
            'type' => 1
        ],
        [
            'code' => 'B05',
            'question' => 'Pekerjaan responden',
            'type' => 1
        ],
        [
            'code' => 'B06',
            'question' => 'Status dalam rumah tangga',
            'type' => 1
        ],
    ],
    'C' => [
        [
            'code' => 'C01',
            'question' => 'Nama Kepala Keluarga',
            'type' => 0
        ],
        [
            'code' => 'C02',
            'question' => 'Jenis Kelamin',
            'type' => 1
        ],
        [
            'code' => 'C03',
            'question' => 'Usia',
            'type' => 0
        ],
        [
            'code' => 'C04',
            'question' => 'Tingkat Pendidikan Terakhir',
            'type' => 1
        ],
        [
            'code' => 'C05',
            'question' => 'Pekerjaan responden',
            'type' => 1
        ],
        [
            'code' => 'C06',
            'question' => 'Jumlah anggota rumah tangga?',
            'type' => 0
        ],
    ],
    'D' => [
        [
            'code' => 'D01',
            'question' => 'Status kepemilikan rumah',
            'type' => 1
        ],
        [
            'code' => 'D02',
            'question' => 'Luas tanah (m<sup>2</sup>)',
            'type' => 0
        ],
        [
            'code' => 'D03',
            'question' => 'Luas bangunan (m<sup>2</sup>)',
            'type' => 0
        ],
        [
            'code' => 'D04',
            'question' => 'Kondisi bangunan',
            'type' => 1
        ],
        [
            'code' => 'D05',
            'question' => 'Kondisi lingkungan',
            'type' => 1
        ],
        [
            'code' => 'D06',
            'question' => 'Lebar akses jalan depan rumah',
            'type' => 1
        ],
        [
            'code' => 'D07',
            'question' => 'Daya listrik',
            'type' => 1
        ],
        [
            'code' => 'D08',
            'question' => 'Peruntukan rumah',
            'type' => 1
        ],
        [
            'code' => 'D09',
            'question' => 'Jika berlangganan PDAM, rumah tangga tersebut masuk katagori berapa?',
            'type' => 1
        ]
    ],
    'E' => [
        [
            'code' => 'E01',
            'question' => 'Berapa total pendapatan rumah tangga/bulan?',
            'type' => 0
        ]
    ],
    'F' => [
        [
            'code' => 'F01',
            'question' => 'Jenis sumber air untuk minum?',
            'type' => 1
        ],
        [
            'code' => 'F02',
            'question' => 'Jumlah air yang dihabiskan dalam satu bulan? (m<sup>3</sup>/bulan)',
            'type' => 0
        ],
        [
            'code' => 'F03',
            'question' => 'Apakah Bapak/Ibu membeli air tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F04',
            'question' => 'Berapa biaya yang dihabiskan untuk membeli air tersebut/bulan?',
            'type' => 0
        ],
        [
            'code' => 'F05',
            'question' => 'Bagaimana pendapat Bapak/Ibu dengan harga tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F06',
            'question' => 'Jenis sumber air untuk masak?',
            'type' => 1
        ],
        [
            'code' => 'F07',
            'question' => 'Jumlah air yang dihabiskan dalam satu bulan? (m<sup>3</sup>/bulan)',
            'type' => 1
        ],
        [
            'code' => 'F08',
            'question' => 'Apakah Bapak/Ibu membeli air tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F09',
            'question' => 'Berapa biaya yang dihabiskan untuk membeli air tersebut/bulan?',
            'type' => 0
        ],
        [
            'code' => 'F10',
            'question' => 'Bagaimana pendapat Bapak/Ibu dengan harga tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F11',
            'question' => 'Jenis sumber air untuk mandi/cuci/kakus?',
            'type' => 1
        ],
        [
            'code' => 'F12',
            'question' => 'Jumlah air yang dihabiskan dalam satu bulan? (m<sup>3</sup>/bulan)',
            'type' => 1
        ],
        [
            'code' => 'F13',
            'question' => 'Apakah Bapak/Ibu membeli air tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F14',
            'question' => 'Berapa biaya yang dihabiskan untuk membeli air tersebut/bulan?',
            'type' => 0
        ],
        [
            'code' => 'F15',
            'question' => 'Bagaimana pendapat Bapak/Ibu dengan harga tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F16',
            'question' => 'Dimana lokasi sumber air untuk minum?',
            'type' => 1
        ],
        [
            'code' => 'F17',
            'question' => 'Jika lokasi di lain tempat, berapa meter jaraknya?',
            'type' => 0
        ],
        [
            'code' => 'F18',
            'question' => 'Bagaimana cara mendapatkan air tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F19',
            'question' => 'Berapa lama total waktu (menit) yang dibutuhkan untuk mendapatkan air tersebut?',
            'type' => 0
        ],
        [
            'code' => 'F20',
            'question' => 'Berapa kali dalam sehari anggota keluarga mengambil air tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F21',
            'question' => 'Dimana lokasi sumber air untuk masak?',
            'type' => 1
        ],
        [
            'code' => 'F22',
            'question' => 'Jika lokasi di lain tempat, berapa meter jaraknya?',
            'type' => 0
        ],
        [
            'code' => 'F23',
            'question' => 'Bagaimana cara mendapatkan air tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F24',
            'question' => 'Berapa lama total waktu (menit) yang dibutuhkan untuk mendapatkan air tersebut?',
            'type' => 0
        ],
        [
            'code' => 'F25',
            'question' => 'Berapa kali dalam sehari anggota keluarga mengambil air tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F26',
            'question' => 'Dimana lokasi sumber air untuk mandi/cuci/kakus?',
            'type' => 1
        ],
        [
            'code' => 'F27',
            'question' => 'Jika lokasi di lain tempat, berapa meter jaraknya?',
            'type' => 0
        ],
        [
            'code' => 'F28',
            'question' => 'Bagaimana cara mendapatkan air tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F29',
            'question' => 'Berapa lama total waktu (menit) yang dibutuhkan untuk mendapatkan air tersebut?',
            'type' => 0
        ],
        [
            'code' => 'F30',
            'question' => 'Berapa kali dalam sehari anggota keluarga mengambil air tersebut?',
            'type' => 1
        ],
        [
            'code' => 'F31',
            'question' => 'Apakah sumber air untuk minum tetap tersedia saat musim kemarau?',
            'type' => 1
        ],
        [
            'code' => 'F32',
            'question' => 'Jika tidak tersedia/terbatas, apa yang anda lakukan untuk memenuhi kebutuhan air?',
            'type' => 1
        ],
        [
            'code' => 'F33',
            'question' => 'Berapa rata - rata biaya yang dikeluarkan akibat permasalahan di atas? (Rupiah)',
            'type' => 0
        ],
        [
            'code' => 'F34',
            'question' => 'Apakah sumber air untuk masak tetap tersedia saat musim kemarau?',
            'type' => 1
        ],
        [
            'code' => 'F35',
            'question' => 'Jika tidak tersedia/terbatas, apa yang anda lakukan untuk memenuhi kebutuhan air?',
            'type' => 1
        ],
        [
            'code' => 'F36',
            'question' => 'Berapa rata - rata biaya yang dikeluarkan akibat permasalahan di atas? (Rupiah)',
            'type' => 0
        ],
        [
            'code' => 'F37',
            'question' => 'Apakah sumber air untuk mandi, cuci, dan kakus tetap tersedia saat musim kemarau?',
            'type' => 1
        ],
        [
            'code' => 'F38',
            'question' => 'Jika tidak tersedia/terbatas, apa yang anda lakukan untuk memenuhi kebutuhan air?',
            'type' => 1
        ],
        [
            'code' => 'F39',
            'question' => 'Berapa rata - rata biaya yang dikeluarkan akibat permasalahan di atas? (Rupiah)',
            'type' => 0
        ],
        [
            'code' => 'F40',
            'question' => 'Bagaimana kualitas sumber air untuk minum di rumah anda?',
            'type' => 1
        ],
        [
            'code' => 'F41',
            'question' => 'Bagaimana kualitas sumber air untuk masak di rumah anda?',
            'type' => 1
        ],
        [
            'code' => 'F42',
            'question' => 'Bagaimana kualitas sumber air untuk mandi/cuci/kakus di rumah anda?',
            'type' => 1
        ],
        [
            'code' => 'F43',
            'question' => 'Apakah anda dan keluarga puas dengan kondisi sumber air di rumah tangga anda saat ini?',
            'type' => 1
        ],
        [
            'code' => 'F44',
            'question' => 'Alasan tidak puas?',
            'type' => 1
        ],
        [
            'code' => 'F45',
            'question' => 'Apakah rumah tangga ini memiliki bak penampungan air (toren air)?',
            'type' => 1
        ],
        [
            'code' => 'F46',
            'question' => 'Kapasitas total bak penampungan air? (liter)',
            'type' => 0
        ]
    ],
    'G' => [
        [
            'code' => 'G01',
            'question' => 'Jika ada Perda (Peraturan Daerah) tingkat pelarangan penggunaan Air Bawah Tanah (ABT), apakah anda berminat menggunakan jasa penyedia air bersih PDAM?',
            'type' => 1
        ],
        [
            'code' => 'G02',
            'question' => 'Berapa biaya rekening per bulan yang akan anda bayarkan? (Rupiah)',
            'type' => 1
        ],
        [
            'code' => 'G03',
            'question' => 'Alasan tidak berminat?',
            'type' => 0
        ]
    ]
];
