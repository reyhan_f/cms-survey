<?php
    return [
        [
            'code' => 'A',
            'name' => 'Identifikasi Lokasi',
            'type' => 0
        ],
        [
            'code' => 'B',
            'name' => 'Identifikasi Responden',
            'type' => 0
        ],
        [
            'code' => 'C',
            'name' => 'Deskripsi Perusahaan',
            'type' => 0
        ],
        [
            'code' => 'D',
            'name' => 'Sumber Air',
            'type' => 0
        ],
        [
            'code' => 'E',
            'name' => 'Tingkat Kepuasan',
            'type' => 0
        ],
        [
            'code' => 'F',
            'name' => 'WTC dan STF',
            'type' => 0
        ]
    ];