<?php
return [
    [
        'code' => 'A',
        'name' => 'Identifikasi Lokasi',
        'type' => 2
    ],
    [
        'code' => 'B',
        'name' => 'Identifikasi Responden',
        'type' => 2
    ],
    [
        'code' => 'C',
        'name' => 'Identitas Kepala Keluarga',
        'type' => 2
    ],
    [
        'code' => 'D',
        'name' => 'Karakteristik Tempat Tinggal',
        'type' => 2
    ],
    [
        'code' => 'E',
        'name' => 'Jumlah Pendapatan Rumah Tangga',
        'type' => 2
    ],
    [
        'code' => 'F',
        'name' => 'Sumber Air Minum Rumah Tangga',
        'type' => 2
    ],
    [
        'code' => 'G',
        'name' => 'WTC dan WTP',
        'type' => 2
    ]
];
