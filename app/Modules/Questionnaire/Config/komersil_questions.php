<?php
return [
        'A' => [
            [
                'code' => 'A01',
                'question' => 'Kota',
                'type' => 0
            ],
            [
                'code' => 'A02',
                'question' => 'Kecamatan',
                'type' => 0
            ],
            [
                'code' => 'A03',
                'question' => 'Kelurahan',
                'type' => 0
            ],
            [
                'code' => 'A04',
                'question' => 'No. Urut Responden',
                'type' => 0
            ],
            [
                'code' => 'A05',
                'question' => 'Nama Perusahaan',
                'type' => 0
            ],
            [
                'code' => 'A06',
                'question' => 'Alamat Lengkap',
                'type' => 0
            ],
            [
                'code' => 'A07',
                'question' => 'No. Telp',
                'type' => 0
            ]
        ],
        'B' => [
            [
                'code' => 'B01',
                'question' => 'Nama Responden',
                'type' => 0
            ],
            [
                'code' => 'B02',
                'question' => 'Jenis Kelamin',
                'type' => 1
            ],
            [
                'code' => 'B03',
                'question' => 'Usia',
                'type' => 0
            ],
            [
                'code' => 'B04',
                'question' => 'Tingkat Pendidikan Terakhir',
                'type' => 1
            ],
            [
                'code' => 'B05',
                'question' => 'Jabatan Responden',
                'type' => 1
            ],
        ],
        'C' => [
            [
                'code' => 'C01',
                'question' => 'Sudah berapa lama perusahaan beroprasi?',
                'type' => 1
            ],
            [
                'code' => 'C02',
                'question' => 'Jenis usaha',
                'type' => 1
            ],
            [
                'code' => 'C03',
                'question' => 'Kategori penginapan',
                'type' => 1
            ],
            [
                'code' => 'C04',
                'question' => 'Luas bangunan? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C05',
                'question' => 'Luas taman/halaman? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C06',
                'question' => 'Jumlah Kamar? (kamar)',
                'type' => 0
            ],
            [
                'code' => 'C07',
                'question' => 'Jumlah tempat tidur? (bed)',
                'type' => 0
            ],
            [
                'code' => 'C08',
                'question' => 'Jumlah Karyawan? (jiwa)',
                'type' => 0
            ],
            [
                'code' => 'C09',
                'question' => 'Rata-rata jumlah pengunjung / bulan',
                'type' => 0
            ],
            [
                'code' => 'C10',
                'question' => 'Luas gedung restaurant? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C11',
                'question' => 'Luas taman/halaman? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C12',
                'question' => 'Jumlah kursi yang disediakan?',
                'type' => 0
            ],
            [
                'code' => 'C13',
                'question' => 'Jumlah Karyawan? (jiwa)',
                'type' => 0
            ],
            [
                'code' => 'C14',
                'question' => 'Rata - rata jumlah pengunjung per bulan? (jiwa/bulan)',
                'type' => 0
            ],
            [
                'code' => 'C15',
                'question' => 'Luas gedung perkantoran? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C16',
                'question' => 'Luas taman/halaman? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C17',
                'question' => 'Jumlah Karyawan? (jiwa)',
                'type' => 0
            ],
            [
                'code' => 'C18',
                'question' => 'Berapa lama jam kerja dalam sehari?',
                'type' => 0
            ],
            [
                'code' => 'C19',
                'question' => 'Luas gedung pasar (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C20',
                'question' => 'Luas taman/halaman? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C21',
                'question' => 'Jumlah pedagang yang berjualan di pasar? (jiwa)',
                'type' => 0
            ],
            [
                'code' => 'C22',
                'question' => 'berapa jam dalam sehari pasar beroperasi?',
                'type' => 0
            ],
            [
                'code' => 'C23',
                'question' => 'Luas gedung sekolah / universitas? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C24',
                'question' => 'Luas taman/halaman? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C25',
                'question' => 'Jumlah Karyawan? (jiwa)',
                'type' => 0
            ],
            [
                'code' => 'C26',
                'question' => 'Jumlah siswa / mahasiswa? (jiwa)',
                'type' => 0
            ],
            [
                'code' => 'C27',
                'question' => 'Luas gedung mall/pusat perbelanjaan? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C28',
                'question' => 'Luas taman/halaman? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C29',
                'question' => 'Jumlah Karyawan? (jiwa)',
                'type' => 0
            ],
            [
                'code' => 'C30',
                'question' => 'Rata - rata jumlah pengunjung per bulan? (jiwa/bulan)',
                'type' => 0
            ],
            [
                'code' => 'C31',
                'question' => 'Luas gedung rumah sakit? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C32',
                'question' => 'Luas taman/halaman? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C33',
                'question' => 'Jumlah Karyawan? (jiwa)',
                'type' => 0
            ],
            [
                'code' => 'C34',
                'question' => 'Kapasitas rawat inap / berapa bed yang tersedia untuk rawat inap?',
                'type' => 0
            ],
            [
                'code' => 'C35',
                'question' => 'Rata - rata jumlah pasien per bulan? (jiwa/bulan)',
                'type' => 0
            ],
            [
                'code' => 'C36',
                'question' => 'Luas wilayah proyek perumahan? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C37',
                'question' => 'Berapa unit rumah yang akan dibangun?',
                'type' => 0
            ],
            [
                'code' => 'C38',
                'question' => 'Berapa perkiraan jumlah penduduknya? (jiwa)',
                'type' => 0
            ],
            [
                'code' => 'C39',
                'question' => 'Luas kawasan industri? (m<sup>2</sup>)',
                'type' => 0
            ],
            [
                'code' => 'C40',
                'question' => 'Jumlah Karyawan? (jiwa)',
                'type' => 0
            ],
            [
                'code' => 'C41',
                'question' => 'Lebar akses jalan depan perusahaan?',
                'type' => 1
            ],
            [
                'code' => 'C42',
                'question' => 'Jika berlangganan PDAM, perusahaan termasuk katagori?',
                'type' => 1
            ],
            [
                'code' => 'C43',
                'question' => 'Tingkat occupancy penginapan? (%)',
                'type' => 0
            ],
        ],
        'D' => [
            [
                'code' => 'D01',
                'question' => 'Apakah di tempat ini menggunakan sumber air dari sumur bor artesis?',
                'type' => 1
            ],
            [
                'code' => 'D02',
                'question' => 'Berapa volume air yang digunakan untuk operasional perusahaan yang berasal dari sumur bor tersebut / bulan? (m<sup>3</sup>/bulan)',
                'type' => 0
            ],
            [
                'code' => 'D03',
                'question' => 'Dimana lokasi sumur artesis tersebut?',
                'type' => 1
            ],
            [
                'code' => 'D04',
                'question' => 'Berapa jarak lokasi sumur dengan perusahaan?',
                'type' => 1
            ],
            [
                'code' => 'D05',
                'question' => 'Apakah lokasi tersebut milik perusahaan?',
                'type' => 1
            ],
            [
                'code' => 'D06',
                'question' => 'Berapa biaya sewa lahan tersebut per bulan? (Rupiah/bulan)',
                'type' => 0
            ],
            [
                'code' => 'D07',
                'question' => 'Berapa biaya investasi pembuatan sumur artesis? (Rupiah)',
                'type' => 0
            ],
            [
                'code' => 'D08',
                'question' => 'Rata - rata biaya pengoperasian perpompaan untuk sumber air per bulan? (Rupiah/bulan)',
                'type' => 0
            ],
            [
                'code' => 'D09',
                'question' => 'Air sumur perlu diolah terlebih dahulu atau tidak sebelum digunakan?',
                'type' => 1
            ],
            [
                'code' => 'D10',
                'question' => 'Apakah perusahaan memiliki IPA sendiri?',
                'type' => 1
            ],
            [
                'code' => 'D11',
                'question' => 'Berapa biaya untuk membangun IPA tersebut? (Rupiah)',
                'type' => 0
            ],
            [
                'code' => 'D12',
                'question' => 'Berapa biaya untuk mengoperasikan instalasi pengolahan air tersebut per bulan? (Rupiah per bulan)',
                'type' => 0
            ],
            [
                'code' => 'D13',
                'question' => 'Berapa biaya pemeliharaan instalasi pengolahan air tersebut per tahun? (Rupiah per tahun)',
                'type' => 0
            ],
            [
                'code' => 'D14',
                'question' => 'Berapa biaya sewa yang perlu dibayar untuk proses pengolahan? (Rupiah per bulan)',
                'type' => 0
            ],
            [
                'code' => 'D15',
                'question' => 'Apakah pemanfaatan sumur dikenakan biaya retribusi?',
                'type' => 1
            ],
            [
                'code' => 'D16',
                'question' => 'Berapa biaya retribusi tersebut? (Rupiah per bulan)',
                'type' => 0
            ],
            [
                'code' => 'D17',
                'question' => 'Apakah di tempat ini menggunakan sumber air dari PDAM?',
                'type' => 1
            ],
            [
                'code' => 'D18',
                'question' => 'Berapa volume air yang digunakan untuk operasional perusahaan yang berasal dari PDAM / bulan? (m<sup>3</sup>/bulan)',
                'type' => 0
            ],
            [
                'code' => 'D19',
                'question' => 'Rata - rata tagihan rekening per bulan yang dibayarkan? (Rupiah/bulan)',
                'type' => 0
            ],
            [
                'code' => 'D20',
                'question' => 'Apakah di tempat ini menggunakan air yang berasal dari proses RO?',
                'type' => 1
            ],
            [
                'code' => 'D21',
                'question' => 'Berapa volume air yang digunakan untuk operasional perusahaan yang berasal dari proses RO / bulan? (m<sup>3</sup>/bulan)',
                'type' => 0
            ],
            [
                'code' => 'D22',
                'question' => 'Berapa biaya untuk membangun IPA berbasis RO tersebut? (Rupiah)',
                'type' => 0
            ],
            [
                'code' => 'D23',
                'question' => 'Berapa biaya untuk mengoperasikan instalasi pengolahan air tersebut per bulan? (Rupiah per bulan)',
                'type' => 0
            ],
            [
                'code' => 'D24',
                'question' => 'Berapa biaya pemeliharaan instalasi pengolahan air tersebut per tahun? (Rupiah per tahun)',
                'type' => 0
            ],
            [
                'code' => 'D25',
                'question' => 'Apakah di tempat ini menggunakan air yang berasal dari proses daur ulang (recycle)?',
                'type' => 1
            ],
            [
                'code' => 'D26',
                'question' => 'Berapa volume air yang digunakan untuk operasional perusahaan yang berasal dari proses recycle / bulan? (m<sup>3</sup>/bulan)',
                'type' => 0
            ],
            [
                'code' => 'D27',
                'question' => 'Berapa biaya untuk membangun IPA berbasis recycle tersebut? (Rupiah)',
                'type' => 0
            ],
            [
                'code' => 'D28',
                'question' => 'Berapa biaya untuk mengoperasikan instalasi pengolahan air tersebut per bulan? (Rupiah per bulan)',
                'type' => 0
            ],
            [
                'code' => 'D29',
                'question' => 'Berapa biaya pemeliharaan instalasi pengolahan air tersebut per tahun? (Rupiah per tahun)',
                'type' => 0
            ],
            [
                'code' => 'D30',
                'question' => 'Apakah di tempat ini menggunakan sumber air dari mobil tangki?',
                'type' => 1
            ],
            [
                'code' => 'D31',
                'question' => 'Berapa volume air yang digunakan untuk operasional perusahaan yang berasal dari mobil tangki / bulan? (m<sup>3</sup>/bulan)',
                'type' => 0
            ],
            [
                'code' => 'D32',
                'question' => 'Rata - rata biaya air yang dikeluarkan per bulan? (Rupiah/bulan)',
                'type' => 0
            ],
            [
                'code' => 'D33',
                'question' => 'Apakah perusahaan ini memiliki bak reservoir air sendiri?',
                'type' => 1
            ],
            [
                'code' => 'D34',
                'question' => 'Berapa kapasitas total reservoir tersebut? (m<sup>3</sup>)',
                'type' => 0
            ],
            [
                'code' => 'D35',
                'question' => 'Berapa jumlah titik bor sumur artesis?',
                'type' => 0
            ],
        ],
        'E' => [
            [
                'code' => 'E01',
                'question' => 'Bagaimana tingkat kepuasan perusahaan terhadap sumber air yang ada saat ini?',
                'type' => 1
            ]
        ],
        'F' => [
            [
                'code' => 'F01',
                'question' => 'Jika ada Perda (Peraturan Daerah) tingkat pelarangan penggunaan Air Bawah Tanah (ABT), apakah anda berminat menggunakan jasa penyedia air bersih PDAM?',
                'type' => 1
            ],
            [
                'code' => 'F02',
                'question' => 'Produk mana yang akan anda pilih?',
                'type' => 1
            ],
            [
                'code' => 'F03',
                'question' => 'Berapa biaya penyambungan yang akan perusahaan bayarkan?',
                'type' => 0
            ],
            [
                'code' => 'F04',
                'question' => 'Tarif per m<sup>3</sup> yang akan perusahaan bayar?',
                'type' => 0
            ],
            [
                'code' => 'F05',
                'question' => 'Alasan tidak berminat?',
                'type' => 1
            ]
        ]
    ];
