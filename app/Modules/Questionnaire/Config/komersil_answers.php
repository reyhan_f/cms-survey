<?php
    return [
        'B02' => [
            [
                'key' => 1,
                'value' => 'Laki-laki'
            ],
            [
                'key' => 2,
                'value' => 'Perempuan'
            ]
        ],
        'B04' => [
            [
                'key' => 1,
                'value' => 'Tidak sekolah'
            ],
            [
                'key' => 2,
                'value' => 'Tidak tamat SD'
            ],
            [
                'key' => 3,
                'value' => 'Tamat SD'
            ],
            [
                'key' => 4,
                'value' => 'Tamat SMP'
            ],
            [
                'key' => 5,
                'value' => 'Tamat SMA'
            ],
            [
                'key' => 6,
                'value' => 'Akademi (D1/D2/D3)'
            ],
            [
                'key' => 7,
                'value' => 'D4/S1/Pasca Sarjana'
            ]
        ],
        'B05' => [
            [
                'key' => 1,
                'value' => 'Pemilik'
            ],
            [
                'key' => 2,
                'value' => 'Direksi'
            ],
            [
                'key' => 3,
                'value' => 'Manager'
            ],
            [
                'key' => 4,
                'value' => 'Asisten Manager'
            ],
            [
                'key' => 5,
                'value' => 'Supervisor'
            ]
        ],
        'C01' => [
            [
                'key' => 1,
                'value' => '< 1 tahun'
            ],
            [
                'key' => 2,
                'value' => '1 - 5 tahun'
            ],
            [
                'key' => 3,
                'value' => '5 - 10 tahun'
            ],
            [
                'key' => 4,
                'value' => '> 10 tahun'
            ]
        ],
        'C02' => [
            [
                'key' => 1,
                'value' => 'Hotel'
            ],
            [
                'key' => 2,
                'value' => 'Restaurant'
            ],
            [
                'key' => 3,
                'value' => 'Gedung perkantoran'
            ],
            [
                'key' => 4,
                'value' => 'Pasar'
            ],
            [
                'key' => 5,
                'value' => 'Sekolah / Universitas'
            ],
            [
                'key' => 6,
                'value' => 'Mall'
            ],
            [
                'key' => 7,
                'value' => 'Rumah Sakit'
            ],
            [
                'key' => 8,
                'value' => 'Proyek Perumahan'
            ],
            [
                'key' => 9,
                'value' => 'Industri'
            ]
        ],
        'C03' => [
            [
                'key' => 1,
                'value' => 'Hotel Berbintang'
            ],
            [
                'key' => 2,
                'value' => 'Hotel non-berbintang'
            ],
            [
                'key' => 3,
                'value' => 'Villa'
            ],
            [
                'key' => 4,
                'value' => 'Guest House'
            ],
            [
                'key' => 5,
                'value' => 'Indekos'
            ],
            [
                'key' => 6,
                'value' => 'Rumah Sewa'
            ]
        ],
        'C41' => [
            [
                'key' => 1,
                'value' => '0 - 6,99 meter'
            ],
            [
                'key' => 2,
                'value' => '> 7 meter'
            ],
        ],
        'C42' => [
            [
                'key' => 1,
                'value' => 'E1'
            ],
            [
                'key' => 2,
                'value' => 'E2'
            ],
            [
                'key' => 3,
                'value' => 'F1'
            ],
            [
                'key' => 4,
                'value' => 'F2'
            ],
            [
                'key' => 5,
                'value' => 'Khusus'
            ]
        ],
        'D01' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ],
        ],
        'D03' => [
            [
                'key' => 1,
                'value' => 'Di dalam kompleks perusahaan'
            ],
            [
                'key' => 2,
                'value' => 'Di luar kompleks perusahaan'
            ]
        ],
        'D04' => [
            [
                'key' => 1,
                'value' => '< 1 km'
            ],
            [
                'key' => 2,
                'value' => '1 - 5 km'
            ],
            [
                'key' => 3,
                'value' => '5 - 10 km'
            ],
            [
                'key' => 4,
                'value' => '> 10 km'
            ]
        ],
        'D05' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak, perusahaan menyewa lokasi'
            ]
        ],
        'D09' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ]
        ],
        'D10' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak, perusahaan menyerahkan proses pengolahan ke pihak ketiga'
            ],
        ],
        'D15' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ],
        ],
        'D17' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ],
        ],
        'D20' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ],
        ],
        'D25' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ],
        ],
        'D30' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ],
        ],
        'D33' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ],
        ],
        'E01' => [
            [
                'key' => 1,
                'value' => 'Sangat tidak puas'
            ],
            [
                'key' => 2,
                'value' => 'Tidak puas'
            ],
            [
                'key' => 3,
                'value' => 'Cukup puas'
            ],
            [
                'key' => 4,
                'value' => 'Puas'
            ],
            [
                'key' => 5,
                'value' => 'Sangat puas'
            ]
        ],
        'F01' => [
            [
                'key' => 1,
                'value' => 'Berminat'
            ],
            [
                'key' => 2,
                'value' => 'Tidak berminat'
            ]
        ],
        'F02' => [
            [
                'key' => 1,
                'value' => 'Produk 1'
            ],
            [
                'key' => 2,
                'value' => 'Produk 2'
            ],
            [
                'key' => 3,
                'value' => 'Produk 3'
            ]
        ],
        'F05' => [
            [
                'key' => 1,
                'value' => 'Masalah biaya penyambungan'
            ],
            [
                'key' => 2,
                'value' => 'Masalah biaya rekening per bulan'
            ],
            [
                'key' => 3,
                'value' => 'Sudah puas dengan sumber air yang ada'
            ],
            [
                'key' => 4,
                'value' => 'Kualitas air PDAM kurang baik'
            ],
            [
                'key' => 5,
                'value' => 'Pelayanan PDAM kurang baik (Ex : aliran sering macet)'
            ]
        ]
    ];
