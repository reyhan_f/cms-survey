<?php
    return [
        'B02' => [
            [
                'key' => 1,
                'value' => 'Laki-laki'
            ],
            [
                'key' => 2,
                'value' => 'Perempuan'
            ]
        ],
        'B04' => [
            [
                'key' => 1,
                'value' => 'Tidak sekolah'
            ],
            [
                'key' => 2,
                'value' => 'Tidak tamat SD'
            ],
            [
                'key' => 3,
                'value' => 'Tamat SD'
            ],
            [
                'key' => 4,
                'value' => 'Tamat SMP'
            ],
            [
                'key' => 5,
                'value' => 'Tamat SMA'
            ],
            [
                'key' => 6,
                'value' => 'Akademi(D1/D2/D3)'
            ],
            [
                'key' => 7,
                'value' => 'D4/S1/Pasca Sarjana'
            ]
        ],
        'B05' => [
            [
                'key' => 1,
                'value' => 'PNS'
            ],
            [
                'key' => 2,
                'value' => 'Pegawai Swasta'
            ],
            [
                'key' => 3,
                'value' => 'TNI/POLRI'
            ],
            [
                'key' => 4,
                'value' => 'Pengusaha'
            ],
            [
                'key' => 5,
                'value' => 'Buruh'
            ],
            [
                'key' => 6,
                'value' => 'Petani'
            ],
            [
                'key' => 7,
                'value' => 'Nelayan'
            ],
            [
                'key' => 8,
                'value' => 'Pelajar'
            ],
            [
                'key' => 9,
                'value' => 'IRT'
            ],
            [
                'key' => 10,
                'value' => 'Tidak bekerja'
            ]
        ],
        'B06' => [
            [
                'key' => 1,
                'value' => 'Kepala rumah tangga'
            ],
            [
                'key' => 2,
                'value' => 'Pasangan KK'
            ],
            [
                'key' => 3,
                'value' => 'Anggota rumah tangga'
            ]
        ],
        'C02' => [
            [
                'key' => 1,
                'value' => 'Laki-laki'
            ],
            [
                'key' => 2,
                'value' => 'Perempuan'
            ]
        ],
        'C04' => [
            [
                'key' => 1,
                'value' => 'Tidak sekolah'
            ],
            [
                'key' => 2,
                'value' => 'Tidak tamat SD'
            ],
            [
                'key' => 3,
                'value' => 'Tamat SD'
            ],
            [
                'key' => 4,
                'value' => 'Tamat SMP'
            ],
            [
                'key' => 5,
                'value' => 'Tamat SMA'
            ],
            [
                'key' => 6,
                'value' => 'Akademi(D1/D2/D3)'
            ],
            [
                'key' => 7,
                'value' => 'D4/S1/Pasca Sarjana'
            ]
        ],
        'C05' => [
            [
                'key' => 1,
                'value' => 'PNS'
            ],
            [
                'key' => 2,
                'value' => 'Pegawai Swasta'
            ],
            [
                'key' => 3,
                'value' => 'TNI/POLRI'
            ],
            [
                'key' => 4,
                'value' => 'Pengusaha'
            ],
            [
                'key' => 5,
                'value' => 'Buruh'
            ],
            [
                'key' => 6,
                'value' => 'Petani'
            ],
            [
                'key' => 7,
                'value' => 'Nelayan'
            ],
            [
                'key' => 8,
                'value' => 'Pelajar'
            ],
            [
                'key' => 9,
                'value' => 'IRT'
            ],
            [
                'key' => 10,
                'value' => 'Tidak bekerja'
            ]
        ],
        'D01' => [
            [
                'key' => 1,
                'value' => 'Milik sendiri'
            ],
            [
                'key' => 2,
                'value' => 'Milik orangtua / keluarga'
            ],
            [
                'key' => 3,
                'value' => 'Sewa / Kontrak'
            ],
            [
                'key' => 4,
                'value' => 'Rumah dinas'
            ]
        ],
        'D04' => [
            [
                'key' => 1,
                'value' => 'Rumah Sangat Sedarhana/Tidak Permanen'
            ],
            [
                'key' => 2,
                'value' => 'Rumah Sederhana'
            ],
            [
                'key' => 3,
                'value' => 'Rumah Sangat Sedarhana/Tidak Permanen'
            ],
            [
                'key' => 4,
                'value' => 'Rumah Sederhana'
            ]
        ],
        'D05' => [
            [
                'key' => 1,
                'value' => 'Gang'
            ],
            [
                'key' => 2,
                'value' => 'Tertata Baik Kavling'
            ],
            [
                'key' => 3,
                'value' => 'Tertata Baik Perumahan'
            ],
            [
                'key' => 4,
                'value' => 'Perumahan Mewah'
            ]
        ],
        'D06' => [
            [
                'key' => 1,
                'value' => '0 - 3,99 meter'
            ],
            [
                'key' => 2,
                'value' => '4 – 6,99 meter'
            ],
            [
                'key' => 3,
                'value' => '> 7 meter'
            ]
        ],
        'D07' => [
            [
                'key' => 1,
                'value' => '450 Watt'
            ],
            [
                'key' => 2,
                'value' => '900 Watt'
            ],
            [
                'key' => 3,
                'value' => '1.300 Watt'
            ],
            [
                'key' => 4,
                'value' => '2.200 Watt'
            ],
            [
                'key' => 5,
                'value' => '> 2.200 Watt'
            ]
        ],
        'D08' => [
            [
                'key' => 1,
                'value' => 'Sebagai tempat tinggal saja'
            ],
            [
                'key' => 2,
                'value' => 'Sebagai tempat tinggal dan usaha/niaga'
            ]
        ],
        'F01' => [
            [
                'key' => 1,
                'value' => '1 bulan s/d 1 tahun'
            ],
            [
                'key' => 2,
                'value' => '1,1 tahun s/d 3 tahun'
            ],
            [
                'key' => 3,
                'value' => '3,1 tahun s/d 5 tahun'
            ],
            [
                'key' => 4,
                'value' => 'Diatas 5 tahun'
            ]
        ],
        'F02' => [
            [
                'key' => 1,
                'value' => 'Rumah tangga ini saja.'
            ],
            [
                'key' => 2,
                'value' => 'Tetangga ikut menggunakannya'
            ]
        ],
        'F03' => [
            [
                'key' => 1,
                'value' => 'Untuk kebutuhan rumah tangga saja'
            ],
            [
                'key' => 2,
                'value' => 'Untuk kebutuhan usaha juga'
            ]
        ],
        'F07' => [
            [
                'key' => 1,
                'value' => 'Murah'
            ],
            [
                'key' => 2,
                'value' => 'Wajar'
            ],
            [
                'key' => 3,
                'value' => 'Mahal'
            ]
        ],
        'F08' => [
            [
                'key' => 1,
                'value' => '≤ 6 jam'
            ],
            [
                'key' => 2,
                'value' => '>6 – ≤12 jam'
            ],
            [
                'key' => 3,
                'value' => '>12 – ≤18 jam'
            ],
            [
                'key' => 4,
                'value' => '>18 – ≤24 jam'
            ]
        ],
        'F09' => [
            [
                'key' => 1,
                'value' => 'Cukup'
            ],
            [
                'key' => 2,
                'value' => 'Tidak mencukupi'
            ]
        ],
        'F10' => [
            [
                'key' => 1,
                'value' => 'Hampir selalu bagus (jernih, tidak berbau dan tidak berasa)'
            ],
            [
                'key' => 2,
                'value' => 'kadang-kadang jernih, kadang-kadang keruh'
            ],
            [
                'key' => 3,
                'value' => 'kadang-kadang bagus kadang-kadang berbau'
            ],
            [
                'key' => 4,
                'value' => 'kadang-kadang tawar, kadang-kadang berasa tidak enak'
            ]
        ],
        'F11' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ]
        ],
        'F12' => [
            [
                'key' => 1,
                'value' => 'Tarif airnya terlalu tinggi'
            ],
            [
                'key' => 2,
                'value' => 'Air sering tidak mengalir'
            ],
            [
                'key' => 3,
                'value' => 'Pasokan air tidak mencukupi'
            ],
            [
                'key' => 4,
                'value' => 'Kualitas air kurang baik/air bau'
            ],
            [
                'key' => 5,
                'value' => 'Lainnya'
            ]
        ],
        'F13' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ]
        ],
        'F14' => [
            [
                'key' => 1,
                'value' => 'Sangat baik dan segera ditanggapi dalam waktu maksimal 2 hari'
            ],
            [
                'key' => 2,
                'value' => 'Baik dan ditanggapi dalam kurun waktu maksimal 7 hari'
            ],
            [
                'key' => 3,
                'value' => 'Kurang ditanggapi dan penanganannya lamban'
            ],
            [
                'key' => 4,
                'value' => 'Lainnya'
            ]
        ],
        'F15' => [
            [
                'key' => 1,
                'value' => 'Mudah mendapatkan air bersih'
            ],
            [
                'key' => 2,
                'value' => 'Air PDAM terjamin kualitasnya'
            ],
            [
                'key' => 3,
                'value' => 'Biayanya murah'
            ],
            [
                'key' => 4,
                'value' => 'Lainnya'
            ]
        ],
        'F16' => [
            [
                'key' => 1,
                'value' => 'Tidak ada saran, pengelolaan PDAM sudah cukup baik'
            ],
            [
                'key' => 2,
                'value' => 'Tempat pembayaran rekening agar ditambah / dipermudah'
            ],
            [
                'key' => 3,
                'value' => 'Keluhan pelanggan agar lebih cepat ditindaklanjuti'
            ],
            [
                'key' => 4,
                'value' => 'Lama pasokan air diperpanjang'
            ],
            [
                'key' => 5,
                'value' => 'Yang rusak cepat diperbaiki'
            ],
            [
                'key' => 6,
                'value' => 'Kualitas air diperbaiki'
            ],
            [
                'key' => 7,
                'value' => 'Lainnya'
            ]
        ],
        'F17' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ]
        ],
        'F18' => [
            [
                'key' => 1,
                'value' => 'Aliran air dari PDAM kurang mencukupi'
            ],
            [
                'key' => 2,
                'value' => 'Kualitas air PDAM kurang baik'
            ],
            [
                'key' => 3,
                'value' => 'Untuk menghemat biaya'
            ],
            [
                'key' => 4,
                'value' => 'Lainnya'
            ]
        ],
        'F19' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ]
        ]
    ];
