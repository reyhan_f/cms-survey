<?php
    return [
        'B02' => [
            [
                'key' => 1,
                'value' => 'Laki-laki'
            ],
            [
                'key' => 2,
                'value' => 'Perempuan'
            ]
        ],
        'B04' => [
            [
                'key' => 1,
                'value' => 'Tidak sekolah'
            ],
            [
                'key' => 2,
                'value' => 'Tidak tamat SD'
            ],
            [
                'key' => 3,
                'value' => 'Tamat SD'
            ],
            [
                'key' => 4,
                'value' => 'Tamat SMP'
            ],
            [
                'key' => 5,
                'value' => 'Tamat SMA'
            ],
            [
                'key' => 6,
                'value' => 'Akademi(D1/D2/D3)'
            ],
            [
                'key' => 7,
                'value' => 'D4/S1/Pasca Sarjana'
            ]
        ],
        'B05' => [
            [
                'key' => 1,
                'value' => 'PNS'
            ],
            [
                'key' => 2,
                'value' => 'Pegawai Swasta'
            ],
            [
                'key' => 3,
                'value' => 'TNI/POLRI'
            ],
            [
                'key' => 4,
                'value' => 'Pengusaha'
            ],
            [
                'key' => 5,
                'value' => 'Buruh'
            ],
            [
                'key' => 6,
                'value' => 'Petani'
            ],
            [
                'key' => 7,
                'value' => 'Nelayan'
            ],
            [
                'key' => 8,
                'value' => 'Pelajar'
            ],
            [
                'key' => 9,
                'value' => 'IRT'
            ],
            [
                'key' => 10,
                'value' => 'Tidak bekerja'
            ]
        ],
        'B06' => [
            [
                'key' => 1,
                'value' => 'Kepala rumah tangga'
            ],
            [
                'key' => 2,
                'value' => 'Pasangan KK'
            ],
            [
                'key' => 3,
                'value' => 'Anggota rumah tangga'
            ]
        ],
        'C02' => [
            [
                'key' => 1,
                'value' => 'Laki-laki'
            ],
            [
                'key' => 2,
                'value' => 'Perempuan'
            ]
        ],
        'C04' => [
            [
                'key' => 1,
                'value' => 'Tidak sekolah'
            ],
            [
                'key' => 2,
                'value' => 'Tidak tamat SD'
            ],
            [
                'key' => 3,
                'value' => 'Tamat SD'
            ],
            [
                'key' => 4,
                'value' => 'Tamat SMP'
            ],
            [
                'key' => 5,
                'value' => 'Tamat SMA'
            ],
            [
                'key' => 6,
                'value' => 'Akademi(D1/D2/D3)'
            ],
            [
                'key' => 7,
                'value' => 'D4/S1/Pasca Sarjana'
            ]
        ],
        'C05' => [
            [
                'key' => 1,
                'value' => 'PNS'
            ],
            [
                'key' => 2,
                'value' => 'Pegawai Swasta'
            ],
            [
                'key' => 3,
                'value' => 'TNI/POLRI'
            ],
            [
                'key' => 4,
                'value' => 'Pengusaha'
            ],
            [
                'key' => 5,
                'value' => 'Buruh'
            ],
            [
                'key' => 6,
                'value' => 'Petani'
            ],
            [
                'key' => 7,
                'value' => 'Nelayan'
            ],
            [
                'key' => 8,
                'value' => 'Pelajar'
            ],
            [
                'key' => 9,
                'value' => 'IRT'
            ],
            [
                'key' => 10,
                'value' => 'Tidak bekerja'
            ]
        ],
        'D01' => [
            [
                'key' => 1,
                'value' => 'Milik sendiri'
            ],
            [
                'key' => 2,
                'value' => 'Milik orangtua / keluarga'
            ],
            [
                'key' => 3,
                'value' => 'Sewa / Kontrak'
            ],
            [
                'key' => 4,
                'value' => 'Rumah dinas'
            ]
        ],
        'D04' => [
            [
                'key' => 1,
                'value' => 'Rumah Sangat Sedarhana/Tidak Permanen'
            ],
            [
                'key' => 2,
                'value' => 'Rumah Sederhana'
            ],
            [
                'key' => 3,
                'value' => 'Rumah Menengah'
            ],
            [
                'key' => 4,
                'value' => 'Rumah Mewah'
            ]
        ],
        'D05' => [
            [
                'key' => 1,
                'value' => 'Gang'
            ],
            [
                'key' => 2,
                'value' => 'Tertata Baik Kavling'
            ],
            [
                'key' => 3,
                'value' => 'Tertata Baik Perumahan'
            ],
            [
                'key' => 4,
                'value' => 'Perumahan Mewah'
            ]
        ],
        'D06' => [
            [
                'key' => 1,
                'value' => '0 - 3,99 meter'
            ],
            [
                'key' => 2,
                'value' => '4 – 6,99 meter'
            ],
            [
                'key' => 3,
                'value' => '> 7 meter'
            ]
        ],
        'D07' => [
            [
                'key' => 1,
                'value' => '450 Watt'
            ],
            [
                'key' => 2,
                'value' => '900 Watt'
            ],
            [
                'key' => 3,
                'value' => '1.300 Watt'
            ],
            [
                'key' => 4,
                'value' => '2.200 Watt'
            ],
            [
                'key' => 5,
                'value' => '> 2.200 Watt'
            ]
        ],
        'D08' => [
            [
                'key' => 1,
                'value' => 'Sebagai tempat tinggal saja'
            ],
            [
                'key' => 2,
                'value' => 'Sebagai tempat tinggal dan usaha/niaga'
            ]
        ],
        'D09' => [
            [
                'key' => 1,
                'value' => 'A1'
            ],
            [
                'key' => 2,
                'value' => 'A2'
            ],
            [
                'key' => 3,
                'value' => 'A3'
            ],
            [
                'key' => 4,
                'value' => 'B'
            ]
        ],
        'F01' => [
            [
                'key' => 1,
                'value' => 'Sumur pompa'
            ],
            [
                'key' => 2,
                'value' => 'Sumur gali'
            ],
            [
                'key' => 3,
                'value' => 'Air kemasan (Botol/Gelas)'
            ],
            [
                'key' => 4,
                'value' => 'Galon bermerk'
            ],
            [
                'key' => 5,
                'value' => 'Galon isi ulang'
            ],
            [
                'key' => 6,
                'value' => 'Truk air/gerobak'
            ],
            [
                'key' => 7,
                'value' => 'Mata air'
            ],
            [
                'key' => 8,
                'value' => 'Sungai/Kali/Kanal'
            ],
            [
                'key' => 9,
                'value' => 'Kolam'
            ],
            [
                'key' => 10,
                'value' => 'Meminta ke tetangga'
            ],
        ],
        'F03' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ]
        ],
        'F05' => [
            [
                'key' => 1,
                'value' => 'Murah'
            ],
            [
                'key' => 2,
                'value' => 'Wajar'
            ],
            [
                'key' => 3,
                'value' => 'Mahal'
            ]
        ],
        'F06' => [
            [
                'key' => 1,
                'value' => 'Sumur pompa'
            ],
            [
                'key' => 2,
                'value' => 'Sumur gali'
            ],
            [
                'key' => 3,
                'value' => 'Air kemasan (Botol/Gelas)'
            ],
            [
                'key' => 4,
                'value' => 'Galon bermerk'
            ],
            [
                'key' => 5,
                'value' => 'Galon isi ulang'
            ],
            [
                'key' => 6,
                'value' => 'Truk air/gerobak'
            ],
            [
                'key' => 7,
                'value' => 'Mata air'
            ],
            [
                'key' => 8,
                'value' => 'Sungai/Kali/Kanal'
            ],
            [
                'key' => 9,
                'value' => 'Kolam'
            ],
            [
                'key' => 10,
                'value' => 'Meminta ke tetangga'
            ],
        ],
        'F07' => [
            [
                'key' => 1,
                'value' => '21 - 25 m3'
            ],
            [
                'key' => 2,
                'value' => '26 - 30 m3'
            ],
            [
                'key' => 2,
                'value' => '31 - 35 m3'
            ],
            [
                'key' => 4,
                'value' => '> 35 m3'
            ],
        ],
        'F08' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ]
        ],
        'F10' => [
            [
                'key' => 1,
                'value' => 'Murah'
            ],
            [
                'key' => 2,
                'value' => 'Wajar'
            ],
            [
                'key' => 3,
                'value' => 'Mahal'
            ]
        ],
        'F11' => [
            [
                'key' => 1,
                'value' => 'Sumur pompa'
            ],
            [
                'key' => 2,
                'value' => 'Sumur gali'
            ],
            [
                'key' => 3,
                'value' => 'Air kemasan (Botol/Gelas)'
            ],
            [
                'key' => 4,
                'value' => 'Galon bermerk'
            ],
            [
                'key' => 5,
                'value' => 'Galon isi ulang'
            ],
            [
                'key' => 6,
                'value' => 'Truk air/gerobak'
            ],
            [
                'key' => 7,
                'value' => 'Mata air'
            ],
            [
                'key' => 8,
                'value' => 'Sungai/Kali/Kanal'
            ],
            [
                'key' => 9,
                'value' => 'Kolam'
            ],
            [
                'key' => 10,
                'value' => 'Meminta ke tetangga'
            ],
        ],
        'F12' => [
            [
                'key' => 1,
                'value' => '21 - 25 m3'
            ],
            [
                'key' => 2,
                'value' => '26 - 30 m3'
            ],
            [
                'key' => 2,
                'value' => '31 - 35 m3'
            ],
            [
                'key' => 4,
                'value' => '> 35 m3'
            ],
        ],
        'F13' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ]
        ],
        'F15' => [
            [
                'key' => 1,
                'value' => 'Murah'
            ],
            [
                'key' => 2,
                'value' => 'Wajar'
            ],
            [
                'key' => 3,
                'value' => 'Mahal'
            ]
        ],
        'F16' => [
            [
                'key' => 1,
                'value' => 'Di Rumah sendiri'
            ],
            [
                'key' => 2,
                'value' => 'Di lain tempat'
            ]
        ],
        'F18' => [
            [
                'key' => 1,
                'value' => 'Diantar sesuai pesanan'
            ],
            [
                'key' => 2,
                'value' => 'Berjalan kaki'
            ],
            [
                'key' => 3,
                'value' => 'Kendaraan non-bermotor'
            ],
            [
                'key' => 4,
                'value' => 'Kendaraan bermotor'
            ]
        ],
        'F20' => [
            [
                'key' => 1,
                'value' => '1 kali'
            ],
            [
                'key' => 2,
                'value' => '2 kali'
            ],
            [
                'key' => 3,
                'value' => '> 3 kali'
            ],
            [
                'key' => 4,
                'value' => 'NA'
            ]
        ],
        'F21' => [
            [
                'key' => 1,
                'value' => 'Di Rumah sendiri'
            ],
            [
                'key' => 2,
                'value' => 'Di lain tempat'
            ]
        ],
        'F23' => [
            [
                'key' => 1,
                'value' => 'Diantar sesuai pesanan'
            ],
            [
                'key' => 2,
                'value' => 'Berjalan kaki'
            ],
            [
                'key' => 3,
                'value' => 'Kendaraan non-bermotor'
            ],
            [
                'key' => 4,
                'value' => 'Kendaraan bermotor'
            ]
        ],
        'F25' => [
            [
                'key' => 1,
                'value' => '1 kali'
            ],
            [
                'key' => 2,
                'value' => '2 kali'
            ],
            [
                'key' => 3,
                'value' => '> 3 kali'
            ],
            [
                'key' => 4,
                'value' => 'NA'
            ]
        ],
        'F26' => [
            [
                'key' => 1,
                'value' => 'Di Rumah sendiri'
            ],
            [
                'key' => 2,
                'value' => 'Di lain tempat'
            ]
        ],
        'F28' => [
            [
                'key' => 1,
                'value' => 'Diantar sesuai pesanan'
            ],
            [
                'key' => 2,
                'value' => 'Berjalan kaki'
            ],
            [
                'key' => 3,
                'value' => 'Kendaraan non-bermotor'
            ],
            [
                'key' => 4,
                'value' => 'Kendaraan bermotor'
            ]
        ],
        'F30' => [
            [
                'key' => 1,
                'value' => '1 kali'
            ],
            [
                'key' => 2,
                'value' => '2 kali'
            ],
            [
                'key' => 3,
                'value' => '> 3 kali'
            ],
            [
                'key' => 4,
                'value' => 'NA'
            ]
        ],
        'F31' => [
            [
                'key' => 1,
                'value' => 'Tersedia'
            ],
            [
                'key' => 2,
                'value' => 'Tidak tersedia / ketersediaan terbatas'
            ]
        ],
        'F32' => [
            [
                'key' => 1,
                'value' => 'Membeli'
            ],
            [
                'key' => 2,
                'value' => 'Meminta ke tetangga'
            ],
            [
                'key' => 3,
                'value' => 'Lainnya'
            ]
        ],
        'F34' => [
            [
                'key' => 1,
                'value' => 'Tersedia'
            ],
            [
                'key' => 2,
                'value' => 'Tidak tersedia / ketersediaan terbatas'
            ]
        ],
        'F35' => [
            [
                'key' => 1,
                'value' => 'Membeli'
            ],
            [
                'key' => 2,
                'value' => 'Meminta ke tetangga'
            ],
            [
                'key' => 3,
                'value' => 'Lainnya'
            ]
        ],
        'F37' => [
            [
                'key' => 1,
                'value' => 'Tersedia'
            ],
            [
                'key' => 2,
                'value' => 'Tidak tersedia / ketersediaan terbatas'
            ]
        ],
        'F38' => [
            [
                'key' => 1,
                'value' => 'Membeli'
            ],
            [
                'key' => 2,
                'value' => 'Meminta ke tetangga'
            ],
            [
                'key' => 3,
                'value' => 'Lainnya'
            ]
        ],
        'F40' => [
            [
                'key' => 1,
                'value' => 'Baik'
            ],
            [
                'key' => 2,
                'value' => 'Kurang baik (berwarna/berasa/berbau)'
            ]
        ],
        'F41' => [
            [
                'key' => 1,
                'value' => 'Baik'
            ],
            [
                'key' => 2,
                'value' => 'Kurang baik (berwarna/berasa/berbau)'
            ]
        ],
        'F42' => [
            [
                'key' => 1,
                'value' => 'Baik'
            ],
            [
                'key' => 2,
                'value' => 'Kurang baik (berwarna/berasa/berbau)'
            ]
        ],
        'F43' => [
            [
                'key' => 1,
                'value' => 'Sangat puas'
            ],
            [
                'key' => 2,
                'value' => 'Puas'
            ],
            [
                'key' => 3,
                'value' => 'Cukup Puas'
            ],
            [
                'key' => 4,
                'value' => 'Tidak Puas'
            ]
        ],
        'F44' => [
            [
                'key' => 1,
                'value' => 'Biaya mahal'
            ],
            [
                'key' => 2,
                'value' => 'Akses mendapatkan airnya sulit'
            ],
            [
                'key' => 3,
                'value' => 'Kualitas air kurang baik'
            ],
            [
                'key' => 4,
                'value' => 'Debit air tidak cukup'
            ]
        ],
        'F45' => [
            [
                'key' => 1,
                'value' => 'Ya'
            ],
            [
                'key' => 2,
                'value' => 'Tidak'
            ]
        ],
        'G01' => [
            [
                'key' => 1,
                'value' => 'Berminat'
            ],
            [
                'key' => 2,
                'value' => 'Tidak berminat'
            ]
        ],
        'G02' => [
            [
                'key' => 1,
                'value' => 'Rp. 150.000 - Rp. 199.000'
            ],
            [
                'key' => 2,
                'value' => 'Rp. 100.000 - Rp. 249.000'
            ],
            [
                'key' => 3,
                'value' => 'Rp. 250.000 - Rp. 299.000'
            ],
            [
                'key' => 4,
                'value' => '> Rp. 300.000'
            ],
        ],
        'G03' => [
            [
                'key' => 1,
                'value' => 'Masalah biaya penyambungan'
            ],
            [
                'key' => 2,
                'value' => 'Masalah biaya rekening per bulan'
            ],
            [
                'key' => 3,
                'value' => 'Sudah puas dengan sumber air yang ada'
            ],
            [
                'key' => 4,
                'value' => 'Kualitas air PDAM kurang baik'
            ],
            [
                'key' => 5,
                'value' => 'Pelayanan PDAM kurang baik (Ex : aliran sering macet )'
            ]
        ],
    ];
