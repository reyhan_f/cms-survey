<?php
return [
    [
        'code' => 'A',
        'name' => 'Identifikasi Lokasi',
        'type' => 1
    ],
    [
        'code' => 'B',
        'name' => 'Identifikasi Responden',
        'type' => 1
    ],
    [
        'code' => 'C',
        'name' => 'Identitas Kepala Keluarga',
        'type' => 1
    ],
    [
        'code' => 'D',
        'name' => 'Karakteristik Tempat Tinggal',
        'type' => 1
    ],
    [
        'code' => 'E',
        'name' => 'Jumlah Pendapatan Rumah Tangga',
        'type' => 1
    ],
    [
        'code' => 'F',
        'name' => 'Sumber Air Minum Rumah Tangga',
        'type' => 1
    ]
];
