<?php

namespace App\Modules\Questionnaire\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    const KOMERSIL = 0;
    const KOMERSIL_STR = 'Komersil';
    const DOMESTIK = 1;
    const DOMESTIK_STR = 'Domestik';
    const DOMESTIK_NP = 2;
    const DOMESTIK_NP_STR = 'Domestik Non Pelanggan';

    protected $fillable = [
        'type', 'code', 'name'
    ];

    protected $appends = [
        'type_str', 'full_name'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * Get the category full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->code}. {$this->name}";
    }

    /**
     * Get the category full name.
     *
     * @return string
     */
    public function getTypeStrAttribute()
    {
        return $this->getTypeStr($this->type);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    /**
     * Function for get string of type
     *
     * @param int $type
     * @return string
     */
    public function getTypeStr(int $type) : string
    {
        switch ($type)
        {
            case 0:
                return $this::KOMERSIL_STR;
            case 1:
                return $this::DOMESTIK_STR;
            default:
                return $this::KOMERSIL_STR;
        }
    }
}
