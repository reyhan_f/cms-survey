<?php

namespace App\Modules\Questionnaire\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'survey_result_id', 'name', 'path'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function survey_result()
    {
        return $this->belongsTo(SurveyResult::class);
    }
}
