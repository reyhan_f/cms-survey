<?php

namespace App\Modules\Questionnaire\Models;

use App\District;
use App\Province;
use App\Regency;
use App\Village;
use Illuminate\Database\Eloquent\Model;

class Summary extends Model
{
    protected $table = 'summaries';

    protected $fillable = [
        'province_id', 'regency_id', 'district_id', 'village_id', 'komersil', 'domestik', 'target'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function village()
    {
        return $this->belongsTo(Village::class);
    }
}
