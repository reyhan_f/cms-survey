<?php

namespace App\Modules\Questionnaire\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyResultDetail extends Model
{
    protected $table = 'survey_result_details';

    protected $fillable = [
        'survey_result_id', 'question_code', 'answer', 'question', 'category'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function survey_result()
    {
        return $this->belongsTo(SurveyResult::class, 'survey_result_id', 'id');
    }

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_code', 'code');
    }
}
