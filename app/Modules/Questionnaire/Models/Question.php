<?php

namespace App\Modules\Questionnaire\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    const ESSAY = 0;
    const ESSAY_STR = 'Essay';
    const MULTIPLE_CHOICES = 1;
    const MULTIPLE_CHOICES_STR = 'Multiple Choices';

    const KOTA = 'A01';
    const KECAMATAN = 'A02';
    const KELURAHAN = 'A03';
    const NO_RESPONDENT_CODE = 'A04';
    const COMPANY = 'A05';
    const ADDRESS_KOMERSIL = 'A06';
    const ADDRESS_DOMESTIK = 'A05';
    const PHONE_KOMERSIL = 'A07';
    const PHONE_DOMESTIK = 'A06';

    const NAME = 'B01';
    const GENDER = 'B02';
    const AGE = 'B03';
    const EDUCATION = 'B04';
    const POSITION = 'B05';
    const GPS = 'coordinate';
    const TIMESTAMP = 'timestamp';
    const TYPE = 'type';

    protected $table = 'questions';

    protected $fillable = [
        'category_id', 'code', 'question', 'type'
    ];

    protected $hidden = [
        'category_id', 'created_at', 'updated_at'
    ];

    protected $appends = [
        'type_str'
    ];

    public function getTypeStrAttribute()
    {
        return $this->getTypeStr($this->type);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function answers()
    {
        return $this->hasMany(Answers::class);
    }

    /**
     * Function for get string of type
     *
     * @param integer $type
     *
     * @return string
     */
    private function getTypeStr(int $type) : string
    {
        switch ($type) {
            case 0:
                return $this::ESSAY_STR;
            case 1:
                return $this::MULTIPLE_CHOICES_STR;
            default:
                return $this::ESSAY_STR;
        }
    }
}
