<?php

namespace App\Modules\Questionnaire\Models;

use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
    protected $table = 'answers';

    protected $fillable = [
        'question_id', 'key', 'value'
    ];

    protected $hidden = [
        'question_id', 'created_at', 'updated_at'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
