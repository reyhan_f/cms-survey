<?php

namespace App\Modules\Questionnaire\Models;

use App\District;
use App\Province;
use App\Regency;
use App\User;
use App\Village;
use Illuminate\Database\Eloquent\Model;

class SurveyResult extends Model
{
    protected $table = 'survey_results';

    protected $fillable = [
        'surveyor_id',
        'respondent_number',
        'name',
        'gender',
        'age',
        'phone',
        'kota',
        'kecamatan',
        'kelurahan',
        'address',
        'education',
        'position',
        'gps',
        'timestamp',
        'type',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function surveyor()
    {
        return $this->belongsTo(User::class, 'surveyor_id', 'id');
    }

    public function detail()
    {
        return $this->hasMany(SurveyResultDetail::class, 'survey_result_id', 'id');
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class, 'kota');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'kecamatan');
    }

    public function village()
    {
        return $this->belongsTo(Village::class, 'kelurahan');
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
