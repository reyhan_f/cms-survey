<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'questionnaire', 'middleware' => 'auth', 'as' => 'questionnaire.'], function () {
    Route::get('/result/{type}/{id}', 'QuestionnaireController@show')->name('result.show');
    Route::get('/result/{type}', 'QuestionnaireController@result')->name('result');
    Route::get('/summary', 'QuestionnaireController@summary')->name('summary');
});
