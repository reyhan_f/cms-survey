<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'API', 'as' => 'v1.'], function () {

    // Datatable
    Route::group(['prefix' => 'datatable/questionnaire', 'as' => 'datatable.questionnaire.', 'middleware' => 'web'], function () {
        Route::get('/summary', 'QuestionnaireController@datatableSummary')->name('summary');
        Route::get('/{type}', 'QuestionnaireController@datatableResult')->name('result');
    });

    // REST API
    Route::group(['prefix' => 'questionnaire', 'as' => 'questionnaire.', 'middleware' => 'auth:api'], function () {
        Route::get('/', 'QuestionnaireController@list')->name('list');
        Route::post('/', 'QuestionnaireController@store')->name('store');
    });

});

Route::group(['prefix' => 'v2', 'namespace' => 'API\V2', 'as' => 'v2.'], function () {

    // REST API
    Route::group(['prefix' => 'questionnaire', 'as' => 'questionnaire.', 'middleware' => 'auth:api'], function () {
        Route::get('/', 'QuestionnaireController@list')->name('list');
        Route::post('/', 'QuestionnaireController@store')->name('store');
    });

});
