@extends('dashboard::layouts.main')

@section('contents')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-copy"></i> <span>Survey Result</span> <small>Detail</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('questionnaire.result', ['type' => $type]) }}"><i class="fa fa-copy"></i> Survey Result</a></li>
            <li>Detail</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="example" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th colspan="3" class="text-center">{{ ucwords($type) }} {{ $respondent_number }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($result as $category => $questions)
                                <tr>
                                    <td colspan="3" class="text-bold">{{ (@$category) ? $category : 'Tambahan' }}</td>
                                </tr>
                                @foreach($questions as $question)
                                    <tr>
                                        @if($question->question)
                                        <td>{{ $question->question_code }}</td>
                                        @endif
                                        <td{{ (@$question->question) ? "" : " colspan=2" }}>{!! (@$question->question) ? $question->question : ucwords($question->question_code) !!}</td>
                                        <td>{{ (@$question->answer) ?: '-' }}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                            <tr>
                                <td colspan="3" class="row">
                                    <div class="col-md-12 col-xs-12">
                                    @forelse($images as $img)
                                        <img class="col-md-3 col-md-3 mr-2 ml-2" src="{{ url($img->path) }}" alt="{{ env('APP_NAME') }}">
                                    @empty
                                        '-'
                                    @endforelse
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <a href="{{ route('questionnaire.result', ['type' => $type]) }}" class="btn btn-primary"><i class="fa fa-backward"></i> <span>Back</span></a>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
