@extends('dashboard::layouts.main')

@section('contents')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-clipboard"></i> <span>Survey Summary</span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('questionnaire.summary') }}"><i class="fa fa-clipboard"></i> Summary</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="summary-survey" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">Provinsi</th>
                                <th class="text-center">Kota/Kabupaten</th>
                                <th class="text-center">Kecamatan</th>
                                <th class="text-center">Komersil per Target</th>
                                <th class="text-center">Domestik per Target</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
    <script>
        $(function () {
            $('#summary-survey').DataTable({
                serverSide: true,
                ajax: '{{ route('v1.datatable.questionnaire.summary') }}',
                columns: [
                    {data: 'provinsi', name: 'provinsi'},
                    {data: 'kota', name: 'kota'},
                    {data: 'kecamatan', name: 'kecamatan'},
                    {data: 'komersil', name: 'komersil', class: 'text-center'},
                    {data: 'domestik', name: 'domestik', class: 'text-center'}
                ]
            });
        });
    </script>
@endsection