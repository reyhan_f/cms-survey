<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'surveyor', 'middleware' => ['auth', 'admin'], 'as' => 'surveyor.'], function () {
    Route::get('/{id}/force-logout', 'SurveyorController@forceLogout')->name('logout');
    Route::get('/{id}/edit', 'SurveyorController@edit')->name('edit');
    Route::post('/{id}/update', 'SurveyorController@update')->name('update');
    Route::get('/{id}/show', 'SurveyorController@show')->name('show');
    Route::post('/{id}/destroy', 'SurveyorController@destroy')->name('destroy');
    Route::get('/create', 'SurveyorController@create')->name('create');
    Route::post('/store', 'SurveyorController@store')->name('store');
    Route::get('/', 'SurveyorController@index')->name('index');
});
