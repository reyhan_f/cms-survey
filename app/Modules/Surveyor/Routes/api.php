<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'API', 'as' => 'v1.'], function () {

    // Datatable
    Route::group(['prefix' => 'surveyor', 'as' => 'surveyor.', 'middleware' => ['auth:api']], function () {
        Route::get('/result/list', 'SurveyorController@resultList')->name('result.list');
        Route::get('/result/{id}', 'SurveyorController@resultShow')->name('result.show');
    });

    // Datatable
    Route::group(['prefix' => 'datatable/questionnaire', 'as' => 'datatable.surveyor.'], function () {
        Route::get('/', 'SurveyorController@datatable')->name('list');
    });

});
