<?php

namespace App\Modules\Surveyor\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('surveyor', 'Resources/Lang', 'app'), 'surveyor');
        $this->loadViewsFrom(module_path('surveyor', 'Resources/Views', 'app'), 'surveyor');
        $this->loadMigrationsFrom(module_path('surveyor', 'Database/Migrations', 'app'), 'surveyor');
        $this->loadConfigsFrom(module_path('surveyor', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('surveyor', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
