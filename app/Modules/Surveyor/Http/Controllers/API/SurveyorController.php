<?php

namespace App\Modules\Surveyor\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Modules\Questionnaire\Models\SurveyResult;
use App\Modules\Questionnaire\Repositories\QuestionsRepository;
use App\User;
use Illuminate\Http\JsonResponse;

class SurveyorController extends Controller
{
    protected $user;
    protected $survey_result;
    protected $repo;

    public function __construct()
    {
        $this->user = new User;
        $this->survey_result = new SurveyResult;
        $this->repo = new QuestionsRepository();
    }

    public function datatable()
    {
        $surveyors = User::all();
        return datatables()->of($surveyors)
            ->editColumn('actions', function ($data) {
                return $this->getDatatableActions($data);
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function getDatatableActions($data)
    {
        $btn_show = '<a href="'. route('surveyor.show', ['id' => encrypt($data->id)]) .'" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>';
        $btn_edit = '<a href="'. route('surveyor.edit', ['id' => encrypt($data->id)]) .'" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-pencil"></i></a>';
        $btn_destroy = '<a href="#" class="btn btn-sm btn-danger confirm-delete" title="Delete" data-toggle="modal" data-target="#confirmDelete" data-url="'.route('surveyor.destroy', ['id' => encrypt(encrypt($data->id))]).'"><i class="fa fa-trash"></i></a>';
        $btn_logout = '';
        if($data->is_login === 1) {
            $btn_logout = '<a href="'.route('surveyor.logout', ['id' => encrypt(encrypt($data->id))]).'" class="btn btn-sm btn-default" title="Force logout"><i class="fa fa-sign-out"></i></a>';
        }
        $actions = '<div class="btn-group">'.$btn_logout.$btn_edit.$btn_destroy.'</div>';
        if(isAdmin(@$data->id)) {
            $actions = '<div class="btn-group">'.$btn_edit.'</div>';
        }
        return $actions;
    }

    /* API */
    /**
     * Get all survey results
     *
     * @return JsonResponse
     */
    public function resultList()
    {
        return response()->json($this->repo->getAllSurveyResult()->toArray());
    }

    /**
     * Show survey results
     *
     * @param int $id
     * @return JsonResponse
     */
    public function resultShow(int $id)
    {
        $result = $this->survey_result->with(['regency.province', 'district', 'village', 'detail'])
            ->where([
                ['surveyor_id', auth()->user()->id],
                ['respondent_number', $id]
            ])
            ->first();
        $result->respondent_number_formated = respondentNumber($result->type, (int) $result->village->id, (int) $result->respondent_number);
        return response()->json($result);
    }
}
