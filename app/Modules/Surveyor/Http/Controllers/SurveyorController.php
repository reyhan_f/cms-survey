<?php

namespace App\Modules\Surveyor\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Surveyor\Http\Requests\StoreRequest;
use App\Modules\Surveyor\Http\Requests\UpdateRequest;
use App\Modules\Surveyor\Models\Surveyor;
use App\User;
use DB;
use Exception;

class SurveyorController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = new User;
    }

    public function index()
    {
        return view('surveyor::index');
    }

    public function create()
    {
        return view('surveyor::create');
    }

    public function store(StoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->only(['name', 'email', 'password', 'phone', 'pin', 'address']);
            $data['password'] = bcrypt($data['password']);
            $user = $this->user->create($data);
            $user->assignRole('surveyor');
            DB::commit();
            toastr()->success('Surveyor has been added.', 'Success');
            return redirect()->route('surveyor.index');
        } catch (Exception $e) {
            DB::rollback();
            toastr()->error($e->getMessage(), 'Error');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $surveyor = Surveyor::find(decrypting($id));
        if(!$surveyor) {
            toastr()->info('Surveyor not found.', 'Failed');
            return redirect()->back();
        }
        return view('surveyor::edit', compact('id', 'surveyor'));
    }

    public function update($id, UpdateRequest $request)
    {
        try {
            $id = decrypting($id);
            DB::beginTransaction();
            $data = $request->only(['name', 'email', 'password', 'phone', 'pin', 'address']);
            if(isAdmin($id)) {
                unset($data['email']);
            }

            if(!is_null($data['password'])) {
                $data['password'] = bcrypt($data['password']);
            } else {
                unset($data['password']);
            }

            $user = $this->user->find($id);
            $user->update($data);
            DB::commit();
            toastr()->success('Surveyor has been updated.', 'Success');
            return redirect()->route('surveyor.index');
        } catch (Exception $e) {
            DB::rollback();
            toastr()->error($e->getMessage(), 'Error');
            return redirect()->back();
        }
    }

    public function show($id)
    {
        $surveyor = Surveyor::find(decrypting($id));
        if(!$surveyor) {
            toastr()->info('Surveyor not found.', 'Failed');
            return redirect()->back();
        }
        return view('surveyor::show', compact('id', 'surveyor'));
    }

    public function destroy()
    {
        try {
            $id = decrypting(decrypting(request('id')));
            DB::beginTransaction();
            if($user = $this->user->find($id)) {
                if(!isAdmin($user->id)) {
                    $user->delete();
                }
            } else {
                DB::rollback();
                toastr()->info('Surveyor not found.', 'Failed');
                return redirect()->back();
            }

            DB::commit();
            toastr()->success('Surveyor has been deleted.', 'Success');
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollback();
            toastr()->error($e->getMessage(), 'Error');
            return redirect()->back();
        }
    }

    public function forceLogout()
    {
        try {
            $id = decrypting(decrypting(request('id')));
            DB::beginTransaction();

            if($user = $this->user->find($id)) {
                $user->update(['is_login' => false]);
                $tokenResult = $user->createToken(env('APP_NAME').' Personal Access Token Force Logout');
                $token = $tokenResult->token;
                $token->save();
            }
            DB::commit();
            toastr()->success('Surveyor has been logged out from current device.', 'Success');
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollback();
            toastr()->error($e->getMessage(), 'Error');
            return redirect()->back();
        }
    }
}
