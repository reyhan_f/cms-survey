<?php

namespace App\Modules\Surveyor\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::check() && isAdmin());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(! isSurveyor(decrypt($this->id))) {
            return $this->general();
        }

        return array_merge($this->general(), $this->admin());
    }

    private function admin()
    {
        return [
            'email' => 'required|email|unique:users,email,'.decrypt($this->id),
        ];
    }

    private function general()
    {
        return [
            'name' => 'required|max:50',
            'phone' => 'required|min:6|unique:users,phone,'.decrypt($this->id),
            'pin' => 'required|integer',
            'address' => 'sometimes|string|max:250',
            'password' => 'sometimes|required_with:password|same:password_confirmation',
        ];
    }
}
