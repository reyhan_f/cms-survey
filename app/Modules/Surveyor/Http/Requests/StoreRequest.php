<?php

namespace App\Modules\Surveyor\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::check() && isAdmin());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|min:6|unique:users,phone',
            'pin' => 'required|integer',
            'address' => 'required|string|max:250',
            'password' => 'required|confirmed|min:8',
        ];
    }
}
