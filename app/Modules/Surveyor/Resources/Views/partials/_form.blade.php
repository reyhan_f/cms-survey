@csrf
<div class="row">
    <div class="col-md-6 col-xs-12">
        <div class="form-group">
            <label for="name">Nama <span class="text-red">*</span></label>
            <input type="text" name="name" class="form-control" value="{{ (@$surveyor->name) ? $surveyor->name : old('name') }}">
            @if ($errors->has('name'))
                <span class="text-red"><small>{{ $errors->first('name') }}</small></span>
            @endif
        </div>
    </div>
    @if(isSurveyor(@$surveyor->id) || isAdmin())
    <div class="col-md-6 col-xs-12">
        <div class="form-group">
            <label for="email">Email <span class="text-red">*</span></label>
            @if(\Request::url() == route('surveyor.create'))
                <input type="email" name="email" class="form-control" value="{{ (@$surveyor->email) ? $surveyor->email : old('email') }}">
            @else
                <input type="email" name="email" class="form-control" value="{{ (@$surveyor->email) ? $surveyor->email : old('email') }}"{{(isSurveyor(@$surveyor->id)) ? '' : ' disabled'}}>
            @endif
            @if ($errors->has('email'))
                <span class="text-red"><small>{{ $errors->first('email') }}</small></span>
            @endif
        </div>
    </div>
    @endif
</div>
<div class="row">
    <div class="col-md-6 col-xs-12">
        <div class="form-group">
            <label for="phone">No. Handphone <span class="text-red">*</span></label>
            <input type="text" name="phone" class="form-control" value="{{ (@$surveyor->phone) ? $surveyor->phone : old('phone') }}">
            @if ($errors->has('phone'))
                <span class="text-red"><small>{{ $errors->first('phone') }}</small></span>
            @endif
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="form-group">
            <label for="pin">PIN <span class="text-red">*</span></label>
            <input type="text" name="pin" class="form-control input_number" max="6" value="{{ (@$surveyor->pin) ? $surveyor->pin : old('pin') }}">
            @if ($errors->has('pin'))
                <span class="text-red"><small>{{ $errors->first('pin') }}</small></span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <label for="phone">Alamat<span class="text-red">*</span></label>
            <input type="text" name="address" class="form-control" value="{{ (@$surveyor->address) ? $surveyor->address : old('address') }}">
            @if ($errors->has('address'))
                <span class="text-red"><small>{{ $errors->first('address') }}</small></span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-xs-12">
        <div class="form-group">
            <label for="password">Password</label>{!! Request::url() === route('surveyor.create') ? ' <span class=text-red>*</span></label>' : '' !!}
            <input type="password" name="password" class="form-control">
            @if ($errors->has('password'))
                <span class="text-red"><small>{{ $errors->first('password') }}</small></span>
            @endif
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="form-group">
            <label for="password">Konfirmasi Password</label>{!! Request::url() === route('surveyor.create') ? ' <span class=text-red>*</span></label>' : '' !!}
            <input type="password" name="password_confirmation" class="form-control">
            @if ($errors->has('password_confirmation'))
                <span class="text-red"><small>{{ $errors->first('password_confirmation') }}</small></span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12 float-sm-left float-md-right">
        <div class="pull-left">
            <a href="{{ route('surveyor.index') }}" class="btn btn-danger">Batal</a>
        </div>
        <div class="pull-right">
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </div>
</div>