@extends('dashboard::layouts.main')

@section('contents')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-user"></i> <span>Surveyor</span> <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#"><i class="fa fa-user"></i> Surveyor</a></li>
            <li><a href="{{ route('surveyor.index') }}"><i class="fa fa-list"></i> List</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="surveyor-list" class="table table-bordered table-hover" style="width: 100%">
                            <thead>
                            <tr>
                                <th class="text-center" width="1%">ID</th>
                                <th class="text-center">Nama</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">No. HP</th>
                                <th class="text-center">Alamat</th>
                                <th class="text-center" width="10%"></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

    <!-- Modal -->
    <div id="confirmDelete" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Konfirmasi Hapus</h4>
                </div>
                <div class="modal-body">
                    <p>Data yang telah di hapus tidak bisa dikembalikan. Apakah Anda yakin mau menghapus data ini?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-danger btn-delete">Hapus</button>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('#surveyor-list').DataTable({
                autoWidth: true,
                serverSide: true,
                ajax: '{{ route('v1.datatable.surveyor.list') }}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'address', name: 'address'},
                    {data: 'actions', name: 'actions', searchable: false, orderable: false, class: 'text-center'},
                ]
            });
            $(document).on('click', '.confirm-delete', function (e) {
                $('#confirmDelete .btn-delete').data('url', $(this).data('url'));
            });
            $(document).on('click', '.btn-delete', function (e) {
                let url = $(this).data('url');
                let form = document.createElement("form");
                let button = document.createElement("button");
                let input = document.createElement("input");
                form.id = 'temp-form';
                form.class = 'temp-form';
                form.method = 'POST';
                form.action = url;
                input.name = '_token';
                input.type = 'hidden';
                input.value = '{{ csrf_token() }}';
                button.id = 'temp-submit';
                button.type = 'submit';
                form.append(input);
                form.append(button);
                document.body.append(form);
                $('#temp-form').trigger('submit').remove();
            });
        });
    </script>
@endsection