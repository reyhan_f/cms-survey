@extends('dashboard::layouts.main')

@section('contents')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-user"></i> <span>Surveyor</span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('surveyor.index') }}"><i class="fa fa-user"></i> Surveyor</a></li>
            <li><a href="{{ route('surveyor.show', ['id' => $id]) }}"><i class="fa fa-user"></i> Ubah Surveyor</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <dl>
                            <dt>Nama</dt>
                            <dd>{{ $surveyor->name }}</dd>
                            <dt>Email</dt>
                            <dd>{{ $surveyor->email }}</dd>
                            <dt>No. Handphone</dt>
                            <dd>{{ $surveyor->phone }}</dd>
                        </dl>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection