@extends('dashboard::layouts.main')

@section('contents')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-user"></i> <span>Surveyor</span> <small>Edit</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('surveyor.index') }}"><i class="fa fa-user"></i> Surveyor</a></li>
            <li><a href="{{ route('surveyor.edit', ['id' => $id]) }}"><i class="fa fa-pencil"></i> Edit</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <form action="{{ route('surveyor.update', ['id' => $id]) }}" method="post" enctype="multipart/form-data">
                            @include('surveyor::partials._form')
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection