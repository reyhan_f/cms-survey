<?php

namespace App\Modules\Surveyor\Models;

use Illuminate\Database\Eloquent\Model;

class Surveyor extends Model
{
    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
