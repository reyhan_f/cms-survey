<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> <span>Home</span></a>
            </li>
            @if(isAdmin())
            <li class="treeview">
                <a href="#"><i class="fa fa-user"></i> <span>Surveyor</span></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('surveyor.index') }}"><i class="fa fa-circle-o"></i> Daftar Surveyor</a></li>
                    <li><a href="{{ route('surveyor.create') }}"><i class="fa fa-circle-o"></i> Tambah Surveyor</a></li>
                </ul>
            </li>
            @endif
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-copy"></i> <span>Survey Result</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('questionnaire.result', ['type' => 'komersil']) }}"><i class="fa fa-circle-o"></i> Komersil</a></li>
                    <li><a href="{{ route('questionnaire.result', ['type' => 'domestik']) }}"><i class="fa fa-circle-o"></i> Domestik</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('questionnaire.summary') }}"><i class="fa fa-clipboard"></i> <span>Summary</span></a>
            </li>
            <li>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i> <span>Sign Out</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>