@extends('dashboard::layouts.main')

@section('contents')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-copy"></i> <span>Survey Results</span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="survey-result" class="table table-bordered table-hover" style="width: 100%">
                            <thead>
                            <tr>
                                <th class="text-center">No. Urut</th>
                                <th class="text-center">Tipe</th>
                                <th class="text-center">Nama</th>
                                <th class="text-center">Jenis Kelamin</th>
                                <th class="text-center">Usia</th>
                                <th class="text-center">Pendidikan</th>
                                <th class="text-center">Jabatan</th>
                                <th class="text-center">Provinsi</th>
                                <th class="text-center">Kota</th>
                                <th class="text-center">Kecamatan</th>
                                <th class="text-center">Kelurahan</th>
                                <th class="text-center"></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
    <script>
        $(function () {
            $('#survey-result').DataTable({
                serverSide: true,
                ajax: '{{ route('v1.datatable.dashboard.result') }}',
                columns: [
                    {data: 'respondent_number', name: 'respondent_number'},
                    {data: 'type', name: 'type'},
                    {data: 'name', name: 'name'},
                    {data: 'gender', name: 'gender'},
                    {data: 'age', name: 'age'},
                    {data: 'education', name: 'education'},
                    {data: 'position', name: 'position'},
                    {data: 'provinsi', name: 'provinsi'},
                    {data: 'kota', name: 'kota'},
                    {data: 'kecamatan', name: 'kecamatan'},
                    {data: 'kelurahan', name: 'kelurahan'},
                    {data: 'action', name: 'action', searchable: false, orderable: false},
                ]
            });
        });
    </script>
@endsection