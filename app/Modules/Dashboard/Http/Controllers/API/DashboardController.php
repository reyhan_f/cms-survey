<?php

namespace App\Modules\Dashboard\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Modules\Questionnaire\Repositories\QuestionsRepository;
use Exception;
use Illuminate\Contracts\Support\Renderable;

class DashboardController extends Controller
{
    protected $repo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->repo = new QuestionsRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     * @throws Exception
     */
    public function datatable()
    {
        $result = $this->repo->getAllSurveyResult();
        return datatables()->of($result)
            ->editColumn('respondent_number', function ($data) {
                return respondentNumber($data->type, (int) $data->village->id, (int) $data->respondent_number);
            })
            ->editColumn('provinsi', function ($data) {
                return $data->regency->province->name;
            })
            ->editColumn('kota', function ($data) {
                return $data->regency->name;
            })
            ->editColumn('kecamatan', function ($data) {
                return $data->district->name;
            })
            ->editColumn('kelurahan', function ($data) {
                return $data->village->name;
            })
            ->editColumn('type', function ($data) {
                return ucwords($data->type);
            })
            ->addColumn('action', function ($data) {
                return '<a href="'.route('questionnaire.result.show', ['type' => $data->type, 'id' => encrypt($data->id)]).'" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>';
            })
            ->rawColumns(['action'])
            ->removeColumn('id')
            ->removeColumn('regency')
            ->removeColumn('district')
            ->removeColumn('village')
            ->toJson();
    }
}
