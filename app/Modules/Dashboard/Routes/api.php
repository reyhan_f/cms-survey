<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'API', 'as' => 'v1.'], function () {

    // Datatable
    Route::group(['prefix' => 'datatable/dashboard', 'as' => 'datatable.dashboard.', 'middleware' => 'web'], function () {
        Route::get('/', 'DashboardController@datatable')->name('result');
    });

});
