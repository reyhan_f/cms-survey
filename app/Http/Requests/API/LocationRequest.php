<?php

namespace App\Http\Requests\API;

use App\Http\Requests\JsonRequest;

class LocationRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:provinces,regencies,districts,villages,province,regency,district,village',
            'code' => 'required_unless:type,provinces'
        ];
    }
}
