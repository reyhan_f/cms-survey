<?php

namespace App\Http\Controllers;

use App\Http\Requests\API\LocationRequest;
use App\Repositories\LocationRepository;

class LocationController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new LocationRepository;
    }

    public function location(LocationRequest $request)
    {
        $type = $request->type;
        $code = $request->code;

        return $this->repo->getLocations($type, $code);
    }
}
