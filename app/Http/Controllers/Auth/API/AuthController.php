<?php

namespace App\Http\Controllers\Auth\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\LoginRequest;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Login user and create token
     *
     * @param LoginRequest $request
     * @return JsonResponse [
     *     access_token,
     *     token_type,
     *     expires_at
     * ]
     */
    public function login(LoginRequest $request)
    {
        $user = User::where([
            'phone' => $request->phone,
            'pin' => $request->pin
        ])->first();
        if(!$user) {
            return response()->json([
                'message' => 'Data yang Anda masukkan salah.'
            ], 401);
        }
        if($user->is_login === 1) {
            return response()->json([
                'message' => 'The user has logged in on another device. Contact admin for further information.'
            ], 403);
        }
        $user->update(['is_login' => true]);
        $tokenResult = $user->createToken($user->phone.$user->pin.env('APP_NAME').' Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'data' => [
                'nama' => $user->name,
                'alamat' => $user->address,
                'phone' => $user->phone,
                'email' => $user->email
            ]
        ]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return JsonResponse [string] message
     */
    public function logout(Request $request)
    {
        $user = User::find($request->user()->id);
        $user->update(['is_login' => false]);
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @param Request $request
     * @return JsonResponse [json] user object
     */
    public function profile(Request $request)
    {
        return response()->json($request->user()->makeHidden(['id', 'pin']));
    }
}
