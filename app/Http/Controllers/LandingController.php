<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;

class LandingController extends Controller
{
    /**
     * @return RedirectResponse
     */
    public function landing()
    {
        return redirect()->route('login');
    }
}
