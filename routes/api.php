<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Auth\API', 'as' => 'v1.'], function () {
    Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
        Route::post('login', 'AuthController@login')->name('login');
        Route::group(['middleware' => 'auth:api'], function() {
            Route::get('profile', 'AuthController@profile')->name('profile');
            Route::get('logout', 'AuthController@logout')->name('logout');
        });
    });
});

Route::group(['prefix' => 'v1'], function () {
    Route::post('location', 'LocationController@location')->name('location');
});

